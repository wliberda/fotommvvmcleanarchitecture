package com.teamdata

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.teamdata.TeamEntity
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 06/02/2018.
 */
@Dao
interface UserTeamJoinDao {
    @Query("SELECT * FROM teams " +
            "INNER JOIN user_team_join ON teams.uuid=user_team_join.teamId WHERE " +
            "user_team_join.userId=:userId")
    fun getTeamsForUser(userId: UUID): Single<List<TeamEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserTeam(userTeamData: UserTeamData)

}
