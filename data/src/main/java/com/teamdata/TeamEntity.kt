package com.teamdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.userdata.UserEntity
import java.util.*

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Entity(tableName = "teams")
class TeamEntity(
        @PrimaryKey
        var uuid: UUID,
        var created: String? = null,
        var changed: String? = null,
        var sync: Boolean = false,
//var listOfFlagTypes: RealmList<FlagTypeRealm>? = null
//var listOfMemberships: RealmList<UserMembershipRealm> = RealmList()
        var teamName: String? = null,
        var isVerified: Boolean = false,
        var teamUrl: String? = null,
        var purpose: String? = null,
//                 var domains: List<String> = emptyList(),
        var surveyCount: Int = 0,
        var flagTypeCount: Int = 0) {
}