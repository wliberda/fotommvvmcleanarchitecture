package com.teamdata

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.base.BaseDAO
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Dao
interface TeamDAO : BaseDAO<TeamEntity> {
    @Query("SELECT * FROM teams WHERE uuid = :uuid")
    fun findTeam(uuid: UUID): Maybe<TeamEntity>

    @Query("SELECT * FROM teams")
    fun findAllTeams(): Single<List<TeamEntity>>



}