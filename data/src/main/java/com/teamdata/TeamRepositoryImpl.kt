package com.teamdata

import com.network.UserAPI
import com.sharedprefrences.Session
import domain.com.team.Team
import domain.com.team.repository.TeamRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class TeamRepositoryImpl(private val teamDAO: TeamDAO,
                         private val userTeamJoinDao: UserTeamJoinDao,
                         private val userAPI: UserAPI,
                         private val mapper: TeamModelMapperImpl,
                         private val session: Session) : TeamRepository {

    override fun addOrEditTeam(team: Team): Completable =
            Completable.fromAction {
                teamDAO.insertOrUpdate(mapper.toEntity(team))
            }


    override fun removeTeam(team: Team): Completable = Completable.fromAction {
        teamDAO.delete(mapper.toEntity(team))
    }


    override fun findByUUID(uuid: UUID): Maybe<Team> =
            teamDAO.findTeam(uuid)
                    .map {
                        mapper.fromEntity(it)
                    }

    override fun getAllTeam(): Single<List<Team>> =
            teamDAO.findAllTeams()
                    .map {
                        it.map(mapper::fromEntity)
                    }

    override fun getTeamForUser(uuid: UUID): Single<List<Team>> =
            Single.concatArray(getCacheTeams(uuid),
                    getRemoteTeams(uuid))
                    .subscribeOn(Schedulers.io())
                    .debounce(500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .singleOrError()


    fun getCacheTeams(uuid: UUID): Single<List<Team>> =
            userTeamJoinDao.getTeamsForUser(session.userUUID)
                    .map {
                        Timber.d("Get cache teams ${it.size}")
                        it.map(mapper::fromEntity)
                    }

    fun getRemoteTeams(uuid: UUID): Single<List<Team>> =
            userAPI.getTeams(session.userUUID)
                    .doOnNext {
                        storeInDB(it.body()!!)

                    }
                    .map {
                        it.body()!!.map(mapper::fromEntity)
                    }
                    .singleOrError()

    private fun storeInDB(list: List<TeamEntity>) {
        Completable.fromCallable {
            list.forEach {
                teamDAO.insertOrUpdate(it)
                userTeamJoinDao.insertUserTeam(UserTeamData(session.userUUID, it.uuid))
            }
        }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe { Timber.d("Inserted teams ${list.size} to DB") }
    }


}