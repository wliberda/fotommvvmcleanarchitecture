package com.teamdata

import domain.com.team.Team
import domain.com.team.repository.TeamModelMapper

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class TeamModelMapperImpl : TeamModelMapper<TeamEntity, Team> {
    override fun fromEntity(from: TeamEntity): Team = from.run {
//        TODO change string url
        Team(uuid, created, changed, sync, teamName, isVerified, teamUrl, purpose, surveyCount,flagTypeCount)
    }


    override fun toEntity(from: Team): TeamEntity = from.run {
        TeamEntity(uuid!!, created, changed, sync, teamName, isVerified, teamUrl, purpose, surveyCount, flagTypeCount)
    }
}