package com.teamdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import com.userdata.UserEntity
import java.util.*

/**
 * Created by wojciechliberda on 06/02/2018.
 */
@Entity(tableName = "user_team_join",
        primaryKeys = ["userId", "teamId"],
        foreignKeys = [
            (ForeignKey(entity = UserEntity::class,
                    parentColumns = ["uuid"],
                    childColumns = ["userId"])),
            (ForeignKey(entity = TeamEntity::class,
                    parentColumns = ["uuid"],
                    childColumns = ["teamId"]))]
)
data class UserTeamData(
        val userId: UUID,
        val teamId: UUID)



