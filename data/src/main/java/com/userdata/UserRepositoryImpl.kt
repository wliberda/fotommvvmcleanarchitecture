package com.userdata

import com.network.UserAPI
import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class UserRepositoryImpl(private val userDAO: UserDAO,
                         private val userAPI: UserAPI,
                         private val mapper: UserModelMapperImpl) : UserRepository {
    override fun addOrEditUser(user: User): Completable = Completable.fromAction {
        userDAO.insertOrUpdate(mapper.toEntity(user))
    }


    override fun deleteUser(user: User): Completable = Completable.fromAction {
        userDAO.delete(mapper.toEntity(user))
    }


    override fun findUserByUUID(uuid: UUID): Maybe<User> =
            userDAO.findUser(uuid)
                    .map { mapper.fromEntity(it) }

    override fun findAllUser(): Single<List<User>> =
            userDAO.findAllUser().map {
                it.map(mapper::fromEntity)
            }

    override fun fetchAndSaveUser(uuid: UUID): Completable =
        userAPI.getUser(uuid, "GET")
                .flatMapCompletable {
                    Completable.fromAction {
                        userDAO.insertOrUpdate(it.body()!!)
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


}