package com.userdata

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.base.BaseDAO

import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
@Dao
interface UserDAO : BaseDAO<UserEntity> {

    @Query("SELECT * FROM users WHERE uuid = :uuid")
    fun findUser(uuid: UUID): Maybe<UserEntity>

    @Query("SELECT * FROM users")
    fun findAllUser(): Single<List<UserEntity>>



}