package com.userdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.mediadata.MediaEntity
import com.teamdata.TeamEntity
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
@Entity(tableName = "users")
data class UserEntity(
        @PrimaryKey()
        var uuid: UUID,
        var created: String? = null,
        var changed: String? = null,
        var firstName: String? = null,
        var sync: Boolean? = false,
        var active: Boolean = false,
        var text: String? = null,
        var lastName: String? = null,
        var email: String? = null,
        var userName: String? = null,
        var occupation: String? = null,
        var attachment: String? = null
//        var photo: MediaEntity? = null)
)