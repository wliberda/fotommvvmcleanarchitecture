package com.userdata

import domain.com.user.User
import domain.com.user.repository.UserModelMapper
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class UserModelMapperImpl : UserModelMapper<UserEntity, User> {
    override fun fromEntity(from: UserEntity): User =
            User(uuid = from.uuid,
                    changed = from.changed,
                    created = from.created,
                    sync = from.sync,
                    active = from.active,
                    firstName = from.firstName,
                    lastName = from.lastName,
                    email = from.email,
                    text = from.text,
                    userName = from.userName,
                    occupation = from.occupation,
                    attachment = from.attachment)


    override fun toEntity(from: User): UserEntity =
            UserEntity(from.uuid!!).apply {
                this.changed= from.changed
                this.created = from.created
                this.sync = from.sync
                this.firstName = from.firstName
                this.lastName = from.lastName
                this.email = from.email
                this.userName = from.userName
                this.occupation = from.occupation
            }


}