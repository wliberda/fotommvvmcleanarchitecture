package com.db

import android.arch.persistence.room.TypeConverter
import java.util.*


/**
 * Created by wojciechliberda on 21/01/2018.
 */
class UUIDConverter {
    @TypeConverter
    fun fromString(value: String): UUID = UUID.fromString(value)

    @TypeConverter
    fun uuidToString(uuid: UUID): String = uuid.toString()
}