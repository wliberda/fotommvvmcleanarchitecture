package com.db

import android.arch.persistence.room.*
import android.content.Context
import com.teamdata.TeamDAO
import com.teamdata.TeamEntity
import com.userdata.UserEntity
import com.teamdata.UserTeamData
import com.teamdata.UserTeamJoinDao
import com.userdata.UserDAO

/**
 * Created by wojciechliberda on 21/01/2018.
 */

@Database(entities = [
    (TeamEntity::class),
    (UserEntity::class),
    (UserTeamData::class)], version = 1, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class FotomDB : RoomDatabase() {
    abstract fun teamDao(): TeamDAO
    abstract fun userDao(): UserDAO
    abstract fun userTeamJoinDao(): UserTeamJoinDao

    companion object {
        var INSTANCE: FotomDB? = null
        fun getInstance(context: Context): FotomDB {
            if(INSTANCE == null)
                synchronized(FotomDB::class) {
                    INSTANCE = Room.databaseBuilder(context, FotomDB::class.java, "fotomdb")
                            .build()
                }
            return INSTANCE!!
        }
    }
}