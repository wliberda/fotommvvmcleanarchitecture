package com.mediadata

import domain.com.media.Media
import domain.com.media.repository.MediaModelMapper

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class MediaModelMapperImpl: MediaModelMapper<MediaEntity, Media> {
    override fun fromEntity(from: MediaEntity): Media =
            from.run {
                Media(uuid, created, changed, sync, mediaType, bucketName, key, size, url, folderName, confirmationToken, awsVerified, active)
            }



    override fun toEntity(from: Media): MediaEntity =
            from.run {
                MediaEntity(uuid, created, changed, sync, mediaType, bucketName, key, size, url, folderName, confirmationToken, awsVerified, active)
            }

}