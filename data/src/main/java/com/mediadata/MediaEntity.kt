package com.mediadata

import com.userdata.UserEntity
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class MediaEntity(
        var uuid: UUID? = null,
        //    @Expose(serialize = false, deserialize = false)
        var created: String? = null,
        var changed: String? = null,
        //    @Expose(serialize = false, deserialize = false)
        var sync: Boolean = false,
        var mediaType: String? = null,
        var bucketName: String? = null,
        var key: String? = null,
        var size: Int = 0,
        var url: String? = null,
        var folderName: String? = null,
        var confirmationToken: String? = null,
        var awsVerified: Boolean = false,
        var active: Boolean = false
)