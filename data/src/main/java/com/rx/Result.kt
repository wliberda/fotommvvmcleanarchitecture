package com.rx

/**
 * Created by wojciechliberda on 10/02/2018.
 */
sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error<out T : Throwable>(val throwable: T) : Result<T>()
}