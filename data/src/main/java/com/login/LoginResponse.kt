package com.login

import java.io.Serializable
import java.util.*

/**
 * Created by wojciechliberda on 07/02/2018.
 */
data class LoginResponse(var expires_in: Int? = null,
                         var access_token: String? =  null,
                         var uuid: UUID? = null,
                         var refresh_token: String? = null,
                         var scope: String? = null,
                         var token_type: String? = null) : Serializable