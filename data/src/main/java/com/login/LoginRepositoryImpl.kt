package com.login

import com.base.Constants
import com.network.UserAPI
import com.sharedprefrences.Session
import domain.com.login.Login
import domain.com.login.repository.LoginRepository
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by wojciechliberda on 07/02/2018.
 */
class LoginRepositoryImpl @Inject constructor(@Named(Constants.LOGIN_USER_API) private val userAPI: UserAPI,
                                              private val session: Session) : LoginRepository {


    override fun logIn(login: Login): Maybe<UUID> =
            userAPI.getRefreshToken(login.grantType!!, login.userName!!, login.password!!)
                    .subscribeOn(Schedulers.io())
                    .singleElement()
                    .map {
                        session.accessToken = it.body()?.access_token!!
                        session.refreshToken = it.body()?.refresh_token!!
                        session.userUUID = it.body()?.uuid!!
                        it.body()?.uuid!!
                    }
                    .observeOn(AndroidSchedulers.mainThread())


}

