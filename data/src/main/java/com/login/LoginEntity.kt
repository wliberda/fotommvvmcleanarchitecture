package com.login

/**
 * Created by wojciechliberda on 08/02/2018.
 */
data class LoginEntity(val grantType: String?,
                 val userName: String?,
                 val password: String?)