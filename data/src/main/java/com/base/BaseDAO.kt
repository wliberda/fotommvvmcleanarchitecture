package com.base

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface BaseDAO<in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(obj: T)

    @Delete
    fun delete(obj: T)
}