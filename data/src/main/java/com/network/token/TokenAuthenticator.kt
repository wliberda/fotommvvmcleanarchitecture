package com.network.token

import com.base.Constants
import com.network.UserAPI
import com.sharedprefrences.Session
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 02/05/2017.
 */
class TokenAuthenticator(private val session: Session, private val userAPI: UserAPI) : Authenticator {


    private val MAX_RETRY = 2

    override fun authenticate(route: Route?, response: Response): Request {
        var count = 0
        do {
            if (response.code() == 401) {
                val newAccessToken = refreshAccessToken()
                ++count
                if (newAccessToken) {
                    return response.request().newBuilder()
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/json")
                            .header("Authorization", "Bearer " + session.accessToken)
                            .build()
                }
            }
        } while (count < MAX_RETRY)
        return response.request()
    }

    private fun refreshAccessToken(): Boolean {
        var isRefresh = false
        userAPI
                .getAccessToken(grant_type = "refresh_token",
                        refresh_token = session.refreshToken,
                        myTeam = session.teamname.toLowerCase())
                .subscribe({
                    it.body()?.let { body ->
                        Timber.d("Success updated access token $it ")
                        body.access_token?.let { session.accessToken = it }
                        body.refresh_token?.let { session.refreshToken = it }
                        isRefresh = true
                    }
                }
                        ,{
                    Timber.e("Failed to refresh access token")
                    isRefresh = false
                })
        return isRefresh
    }
}