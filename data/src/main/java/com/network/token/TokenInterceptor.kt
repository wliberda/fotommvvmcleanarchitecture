package com.network.token

import com.sharedprefrences.Session
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by wojciechliberda on 01/05/2017.
 */
class TokenInterceptor(private val session: Session): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()
                .header("Accept", "application/json")
                .header("Content-Type","application/json")
                .header("Authorization", "Bearer " +  session.accessToken)
                .method(original.method(), original.body())
        val request = requestBuilder.build()
        return chain.proceed(request)
    }

}