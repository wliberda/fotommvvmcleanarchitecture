package com.network

import java.net.ConnectException

/**
 * Created by wojciechliberda on 13/02/2018.
 */
class NetworkException(message: String): ConnectException(message)
