package com.network

import com.login.LoginResponse
import com.teamdata.TeamEntity
import com.userdata.UserEntity
import domain.com.login.Login
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import java.util.*

/**
 * Created by wojciechliberda on 07/02/2018.
 */
interface UserAPI {

    @FormUrlEncoded
    @POST("token")
    fun getRefreshToken(@Field("grant_type") grantType: String,
                        @Field("username") username: String,
                        @Field("password") password: String): Flowable<Response<LoginResponse>>


    @FormUrlEncoded
    @POST("token")
    fun getAccessToken(@Field("grant_type") grant_type: String,
                       @Field("refresh_token") refresh_token: String,
                       @Field("scope") myTeam: String): Observable<Response<LoginResponse>>

    @GET("users/{userId}")
    fun getUser(@Path("userId") uuid: UUID, @Query("mediaUrlType")
    mediaUrlType: String? = null): Flowable<Response<UserEntity>>

    @GET("users/{userID}/teams")
    fun getTeams(@Path("userID") uuid: UUID): Flowable<Response<List<TeamEntity>>>


}