package com.sharedprefrences

import android.content.Context

import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 07/02/2018.
 */
@Singleton
class Session @Inject constructor(_context: Context) {

    private val USER_UUID = "USER_UUID"
    private val TEAM_UUID = "TEAM_UUID"
    private val TEAM_NAME = "TEAM_NAME"
    private val USER_NAME = "USER_NAME"
    private val ACCESS_TOKEN = "ACCESS_TOKEN"
    private val REFRESH_TOKEN = "REFRESH_TOKEN"
    private val FIRST_NAME = "FIRST_NAME"
    private val LAST_NAME = "LAST_NAME"
    private val EMAIL = "EMAIL"
    private val PHOTO_DATE = "PHOTO_DATE"
    private val FLAG_UUID = "FLAG_UUID"
    private val PERMISSIONS = "PERMISSIONS"
    private val FLAG_SCALE = "FLAG_SCALE"
    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context)


    internal var PRIVATE_MODE = 0
    val userUUDString: String
        get() = preferences.getString(USER_UUID, "")
    val teamUUIDString: String
        get() = preferences.getString(TEAM_UUID, "")

    var userUUID: UUID
        get() {
            val userUUID = preferences.getString(USER_UUID, "")
            return UUID.fromString(userUUID)
        }
        set(value) {
            preferences.edit().putString(USER_UUID, value.toString()).apply()
        }

    var teamUUID: UUID
        get() {
            val teamUUID = preferences.getString(TEAM_UUID, "")
            return UUID.fromString(teamUUID)
        }
        set(value) {
            preferences.edit().putString(TEAM_UUID, value.toString()).apply()
        }

    var username: String
        get() = preferences.getString(USER_NAME, "")
        set(value) {
            preferences.edit().putString(USER_NAME, value).apply()
        }

    var teamname: String
        get() = preferences.getString(TEAM_NAME, "")
        set(value) {
            preferences.edit().putString(TEAM_NAME, value).apply()
        }

    var accessToken: String
        get() = preferences.getString(ACCESS_TOKEN, "")
        set(value) {
            preferences.edit().putString(ACCESS_TOKEN, value).apply()
        }

    var refreshToken: String
        get() = preferences.getString(REFRESH_TOKEN, "")
        set(value) {
            preferences.edit().putString(REFRESH_TOKEN, value).apply()
        }
    var firstName: String
        get() = preferences.getString(FIRST_NAME, "")
        set(value) {
            preferences.edit().putString(FIRST_NAME, value).apply()
        }
    var lastName: String
        get() = preferences.getString(LAST_NAME, "")
        set(value) {
            preferences.edit().putString(LAST_NAME, value).apply()
        }


    var email: String
        get() = preferences.getString(EMAIL, "")
        set(value) {
            preferences.edit().putString(EMAIL, value).apply()
        }


    var defaultFlag: String
        get() = preferences.getString(FLAG_UUID, "")
        set(value) {
            preferences.edit().putString(FLAG_UUID, value).apply()
        }

    var flagScale: Float
        get() = preferences.getFloat(FLAG_SCALE, 1f)
        set(value) {
            preferences.edit().putFloat(FLAG_SCALE, value).apply()
        }

    fun logoutUser() {
        preferences.edit().clear().apply()
//        val intent = Intent(_context, LoginResponse::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        _context.startActivity(intent)

    }

    fun deleteSessionData() {
        preferences.edit().clear().apply()
    }

}
