package com.login

import com.network.NetworkException
import com.network.UserAPI
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import com.sharedprefrences.Session
import domain.com.login.Login
import domain.com.login.repository.LoginRepository
import io.reactivex.Flowable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.mock
import retrofit2.Response
import java.util.*

/**
 * Created by wojciechliberda on 13/02/2018.
 */
class LoginRepositoryImplTest {
    companion object {
        val access_token = "access_token"
        val refresh_token = "refreh_token"
        val uuid = UUID.randomUUID()
    }

    lateinit var testSubject: LoginRepository
    val userAPI: UserAPI = mock(UserAPI::class.java)
    val session: Session = mock(Session::class.java)
    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { it -> Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
        testSubject = LoginRepositoryImpl(userAPI, session)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
    }

    @Test
    fun `subject Test shouldnt be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `given login when validLogin passed then return 200`() {

//        whenever(userAPI.getRefreshToken(any(), any(), any())).thenReturn(Flowable.just(Response.success(getLoginResponse())))
//        val testObserver = testSubject.logIn(getValidLogin()).test()
//        testObserver.assertComplete()
    }

    @Test
    fun `given login when code 400 return then WrongPasswordLoginException occurs`() {
//        whenever(userAPI.getRefreshToken(any(), any(), any())).thenReturn(Flowable.just(Response.error(400, mock(ResponseBody::class.java))))
//        val testObserver = testSubject.logIn(getValidLogin()).test()
//        testObserver.assertError(WrongUserNamePasswordException::class.java)
    }

    @Test
    fun `given login when error occurs then return RestException`() {
//        whenever(userAPI.getRefreshToken(any(), any(), any())).thenReturn(Flowable.just(Response.error(404, mock(ResponseBody::class.java))))
//        val testObserver = testSubject.logIn(getValidLogin()).test()
//        testObserver.assertError(NetworkException::class.java)
    }

    fun getLoginResponse() = LoginResponse(1, access_token, uuid, refresh_token, "","")

    fun getValidLogin() = Login("password", "wliberda@omnis.pl", "password")

}