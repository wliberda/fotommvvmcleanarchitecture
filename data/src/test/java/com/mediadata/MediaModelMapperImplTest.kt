package com.mediadata

import domain.com.media.Media
import domain.com.media.repository.MediaModelMapper
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class MediaModelMapperImplTest {

    companion object {
        val uuid = UUID.randomUUID()
        val created = "created"
        val changed = "changed"
        val sync = true
        val mediaType = "mediaTypa"
        val bucketName = "bucketName"
        val key = "key"
        val size = 2
        val url = "url"
        val folderName = "folderName"
        val confirmationToken = "confirmationToken"
        val awsVerified = true
        val active = true
    }

    lateinit var testSubject: MediaModelMapperImpl

    @Before
    fun setUp() {
        testSubject = MediaModelMapperImpl()
    }

    @Test
    fun subjectShouldNotBeNull() {
        assertNotNull(testSubject)
    }

    @Test
    fun testFromEntity() {
//        GIVEN
        val entity = entity()
//        WHEN
        val model = testSubject.fromEntity(entity)
//        THEN
        assert(model.uuid == entity.uuid)
        assert(model.created == entity.created)
        assert(model.changed == entity.changed)
        assert(model.sync == entity.sync)
        assert(model.mediaType == entity.mediaType)
        assert(model.bucketName == entity.bucketName)
        assert(model.key == entity.key)
        assert(model.size == entity.size)
        assert(model.url == entity.url)
        assert(model.folderName == entity.folderName)
        assert(model.confirmationToken == entity.confirmationToken)
        assert(model.awsVerified == entity.awsVerified)
        assert(model.active == entity.active)
    }


    @Test
    fun testFromModel() {
//        GIVEN
        val model = media()
//        WHEN
        val entity = testSubject.toEntity(model)
//        THEN
        assert(model.uuid == entity.uuid)
        assert(model.created == entity.created)
        assert(model.changed == entity.changed)
        assert(model.sync == entity.sync)
        assert(model.mediaType == entity.mediaType)
        assert(model.bucketName == entity.bucketName)
        assert(model.key == entity.key)
        assert(model.size == entity.size)
        assert(model.url == entity.url)
        assert(model.folderName == entity.folderName)
        assert(model.confirmationToken == entity.confirmationToken)
        assert(model.awsVerified == entity.awsVerified)
        assert(model.active == entity.active)
    }


    fun media() = Media(uuid, created, changed, sync, mediaType, bucketName, key, size, url, folderName, confirmationToken, awsVerified, active)

    fun entity() = MediaEntity(uuid, created, changed, sync, mediaType, bucketName, key, size, url, folderName, confirmationToken, awsVerified, active)
}