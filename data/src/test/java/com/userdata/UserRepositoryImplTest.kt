package com.userdata

import com.network.UserAPI
import com.nhaarman.mockito_kotlin.*
import domain.com.user.User
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import retrofit2.Response
import java.util.*

/**
 * Created by wojciechliberda on 03/02/2018.
 */
class UserRepositoryImplTest {

    companion object {
        val UUID = java.util.UUID.randomUUID()
    }

    private val userDaoImpl: UserDAOImpl = mock()

    private val userMapper: UserModelMapperImpl = mock()

    private val userAPI: UserAPI = mock()

    lateinit var testSubject: UserRepositoryImpl

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { it -> Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
        testSubject = UserRepositoryImpl(userDaoImpl, userAPI, userMapper)
    }

    @After
    fun tearDown() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    @Test
    fun subjectCannotBeNull() {
        assertNotNull(testSubject)
    }



    @Test
    fun insertOrUpdate_emitsOnSuccess() {
        val user = user()
        val userEntity = userEntity()

        val testObserver = testSubject.addOrEditUser(user).test()

        testObserver.assertComplete()

        argumentCaptor<UserEntity>().apply {
            verify(userDaoImpl).insertOrUpdate(capture())
            assertEquals(user.uuid, userEntity.uuid)
        }
    }

    @Test
    fun delete_emitsOnSucces() {
        val user = user()
        val userEntity = userEntity()

        val testObserver = testSubject.deleteUser(user).test()
        testObserver.assertComplete()

        argumentCaptor<UserEntity>().apply {
            verify(userDaoImpl).delete(capture())
            assertEquals(user.uuid, userEntity.uuid)
        }

    }

    @Test
    fun findById_givenItemFound_emitsUser() {
//        GIVEN
        val userEntity = userEntity()
        val user = given_userFromEntity(userEntity())
        whenever(userDaoImpl.findUser(any())).thenReturn(Maybe.just(userEntity))
//        WHEN
        val testObserver = testSubject.findUserByUUID(UUID).test()

//        THEN
        testObserver.assertResult(user)

        argumentCaptor<UUID>().apply {
            verify(userDaoImpl).findUser(capture())
            assert(firstValue == user.uuid)
        }
        verify(userMapper).fromEntity(userEntity)
    }


    @Test
    fun findById_giveItemNotFound_emitsEmpty() {
//        GIVEN
        whenever(userDaoImpl.findUser(any())).thenReturn(Maybe.empty())
//        WHEN
        val testObserver = testSubject.findUserByUUID(UUID).test()
//        THEN
        testObserver.assertComplete()
        testObserver.assertNoValues()
    }

    @Test
    fun findAllUsers_givenItemsFound_emitsListOfUsers() {
//        GIVEN
        val user = given_userFromEntity(userEntity())
        val list = listOf(userEntity(), userEntity())
        whenever(userDaoImpl.findAllUser()).thenReturn(Single.just(list))
//        WHEN
        val testObserver = testSubject.findAllUser().test()
//        THEN
        testObserver.assertResult(listOf(user, user))
        testObserver.assertComplete()
    }


    @Test
    fun findAllUsers_givenItemsNotFound_emitsEmptyList() {
//        GIVEN
        whenever(userDaoImpl.findAllUser()).thenReturn(Single.just(emptyList()))
//        WHEN
        val testObserver = testSubject.findAllUser().test()
//        THEN
        testObserver.assertResult(emptyList())
        testObserver.assertComplete()

    }

    @Test
    fun shouldCompleteWhenFetchUserAndSaveInDatabase() {
        val user = user()
        val userEntity = userEntity()
        whenever(userAPI.getUser(any(), any())).thenReturn(Flowable.just(Response.success(userEntity)))

        val testObserver = testSubject.fetchAndSaveUser(UUID).test()

        testObserver.assertComplete()

        argumentCaptor<UserEntity>().apply {
            verify(userDaoImpl).insertOrUpdate(capture())
            assertEquals(user.uuid, userEntity.uuid)
        }

    }



    private fun given_userFromEntity(userEntity: UserEntity): User {
        val user: User = mock()
        whenever(userMapper.fromEntity(userEntity)).thenReturn(user)
        whenever(user.uuid).thenReturn(userEntity.uuid)
        return user
    }


    private fun user() = User(UUID)
    private fun userEntity() = UserEntity(UUID)
}

