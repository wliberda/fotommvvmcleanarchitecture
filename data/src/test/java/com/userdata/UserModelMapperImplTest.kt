package com.userdata

import domain.com.user.User
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class UserModelMapperImplTest {
    private lateinit var testSubject: UserModelMapperImpl

    companion object {
        val uuid = UUID.randomUUID()
        val created = "created"
        val changed = "changed"
        val firstName = "firstName"
        val lastName = "lastName"
        val userName = "userName"
        val email = "email@email.com"
        val active = true
        val sync = true
        val text = "text"
        val occupation = "occupation"
    }

    @Before
    fun setUp(){
        testSubject = UserModelMapperImpl()
    }


    private fun getEntity() : UserEntity =
            UserEntity(uuid, created, changed, firstName, sync, active, text, lastName, email, userName, occupation)

    private fun getModel(): User =
            User(uuid, changed, created, sync, active, firstName, lastName, email, text, userName, occupation)

    @Test
    fun testFromEntity(){
//        GIVEN
        val entity = getEntity()

//        WHEN
        val model = testSubject.fromEntity(entity)

//        THEN
        assertTrue(model.uuid == entity.uuid)
        assertTrue(model.changed == entity.changed)
        assertTrue(model.created == entity.created)
        assertTrue(model.sync == entity.sync)
        assertTrue(model.userName == entity.userName)
        assertTrue(model.firstName == entity.firstName)
        assertTrue(model.lastName == entity.lastName)
        assertTrue(model.occupation == entity.occupation)
        assertTrue(model.email == entity.email)

    }


    @Test
    fun testToEntity(){
//        GIVEN
        val model = getModel()

//        WHEN
        val entity = testSubject.toEntity(model)

//        THEN
        assertTrue(model.uuid == entity.uuid)
        assertTrue(model.changed == entity.changed)
        assertTrue(model.created == entity.created)
        assertTrue(model.sync == entity.sync)
        assertTrue(model.userName == entity.userName)
        assertTrue(model.firstName == entity.firstName)
        assertTrue(model.lastName == entity.lastName)
        assertTrue(model.occupation == entity.occupation)
        assertTrue(model.email == entity.email)

    }
}