package com.teamdata

import domain.com.team.Team
import domain.com.team.repository.TeamModelMapper
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class TeamModelMapperImplTest {

    companion object {
        val uuid = UUID.randomUUID()
        val created = "created"
        val changed = "changed"
        val sync = true
        val teamName = "teamName"
        val isVerified = true
        val teamUrl = "teamUrl"
        val purpose = "purpose"
        val surveyCount = 2
        val flagTypeCount = 3
    }

    lateinit var testSubject: TeamModelMapperImpl

    @Before
    fun setUp() {
        testSubject = TeamModelMapperImpl()
    }


    private fun getModel(): Team =
            Team(uuid, created, changed, sync, teamName, isVerified, teamUrl, purpose, surveyCount, flagTypeCount)


    private fun getEntity(): TeamEntity =
            TeamEntity(uuid, created, changed, sync, teamName, isVerified, teamUrl, purpose, surveyCount, flagTypeCount)

    @Test
    fun shouldNotBeNull() {
        assertNotNull(testSubject)
    }

    @Test
    fun testFromEntity() {
//        GIVEN
        val entity = getEntity()
//        WHEN
        val model = testSubject.fromEntity(entity)

//        THEN
        assertTrue(model.uuid == entity.uuid)
        assertTrue(model.changed == entity.changed)
        assertTrue(model.created == entity.created)
        assertTrue(model.sync == entity.sync)
        assertTrue(model.teamName == entity.teamName)
        assertTrue(model.isVerified == entity.isVerified)
        assertTrue(model.teamUrl == entity.teamUrl)
        assertTrue(model.purpose == entity.purpose)
        assertTrue(model.surveyCount == entity.surveyCount)
        assertTrue(model.flagTypeCount == entity.flagTypeCount)
    }

    @Test
    fun xtestToEntity() {
//        GIVEN
        val model = getModel()
//        WHEN
        val entity = testSubject.toEntity(model)
//        THEN
        assertTrue(model.uuid == entity.uuid)
        assertTrue(model.uuid == entity.uuid)
        assertTrue(model.changed == entity.changed)
        assertTrue(model.created == entity.created)
        assertTrue(model.sync == entity.sync)
        assertTrue(model.teamName == entity.teamName)
        assertTrue(model.isVerified == entity.isVerified)
        assertTrue(model.teamUrl == entity.teamUrl)
        assertTrue(model.purpose == entity.purpose)
        assertTrue(model.surveyCount == entity.surveyCount)
        assertTrue(model.flagTypeCount == entity.flagTypeCount)
        
    }


}
