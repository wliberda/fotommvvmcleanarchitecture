package domain.com.photosetmembership.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoSetMembershipModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}