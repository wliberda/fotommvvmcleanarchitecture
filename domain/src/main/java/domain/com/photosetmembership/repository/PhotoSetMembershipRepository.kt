package domain.com.photosetmembership.repository

import domain.com.photoset.PhotoSet
import domain.com.photosetmembership.PhotoSetMembership
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoSetMembershipRepository {
    fun addOrEditMembership(photoSetMembership: PhotoSetMembership): Completable
    fun removeMembership(photoSetMembership: PhotoSetMembership): Completable
    fun findMembershipByUUID(uuid: UUID): Maybe<PhotoSetMembership>
    fun getAllMembership(): Single<List<PhotoSetMembership>>
}