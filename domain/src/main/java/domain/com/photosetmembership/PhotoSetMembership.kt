package domain.com.photosetmembership

import domain.com.photoset.PhotoSet
import domain.com.user.User

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class PhotoSetMembership(var user: User? = null,
                              var photoSets: PhotoSet? = null,
                              var roleType: PhotoSetsRoleType? = null) {
    enum class PhotoSetsRoleType {
        MANAGER, MEMBER, GUEST
    }


    enum class PhotoSetsPermission {
        VIEW_SURVEY_CONTENT, CREATE_CONTENT, ADMINISTER_CONTENT, INVITE_PEOPLE
    }
}