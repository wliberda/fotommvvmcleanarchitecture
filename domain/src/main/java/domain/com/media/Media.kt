package domain.com.media

import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Media(val uuid: UUID? = null,
                 val created: String? = null,
                 val changed: String? = null,
                 val sync: Boolean = false,
                 var mediaType: String? = null,
                 var bucketName: String? = null,
                 var key: String? = null,
                 var size: Int = 0,
                 var url: String? = null,
                 var folderName: String? = null,
                 var confirmationToken: String? = null,
                 var awsVerified: Boolean = false,
                 var active: Boolean = false) {
}