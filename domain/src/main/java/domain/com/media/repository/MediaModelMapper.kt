package domain.com.media.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface MediaModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}