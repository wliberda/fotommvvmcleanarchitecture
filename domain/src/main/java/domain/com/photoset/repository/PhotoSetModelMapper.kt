package domain.com.photoset.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoSetModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}