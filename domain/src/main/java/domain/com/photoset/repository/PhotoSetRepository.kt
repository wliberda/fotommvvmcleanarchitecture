package domain.com.photoset.repository

import domain.com.photo.Photo
import domain.com.photoset.PhotoSet
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoSetRepository {
    fun addOrEditPhotoSet(photoSet: PhotoSet): Completable
    fun removePhotoSet(photoSet: PhotoSet): Completable
    fun findPhotoSet(uuid: UUID): Maybe<PhotoSet>
    fun getAllPhotoSet(): Single<List<PhotoSet>>
}