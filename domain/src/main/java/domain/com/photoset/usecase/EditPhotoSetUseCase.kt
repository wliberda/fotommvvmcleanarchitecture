package domain.com.photoset.usecase

import domain.com.photoset.PhotoSet
import domain.com.photoset.repository.PhotoSetRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditPhotoSetUseCase(val repository: PhotoSetRepository) {
    fun edit(photoSet: PhotoSet): Completable = valid(photoSet).andThen(repository.addOrEditPhotoSet(photoSet))
    fun valid(photoSet: PhotoSet): Completable = when(photoSet.isValidForEdit()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("photoset validation failed before edit"))
    }
}