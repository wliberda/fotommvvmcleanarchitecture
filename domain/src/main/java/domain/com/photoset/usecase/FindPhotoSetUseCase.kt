package domain.com.photoset.usecase

import domain.com.photoset.repository.PhotoSetRepository
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindPhotoSetUseCase(val repository: PhotoSetRepository) {
    fun find(uuid: UUID) = repository.findPhotoSet(uuid)
}