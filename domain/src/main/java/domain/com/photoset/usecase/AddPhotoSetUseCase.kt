package domain.com.photoset.usecase

import domain.com.photoset.PhotoSet
import domain.com.photoset.repository.PhotoSetRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddPhotoSetUseCase(val repository: PhotoSetRepository) {
    fun add(photoSet: PhotoSet): Completable = valid(photoSet).andThen(repository.addOrEditPhotoSet(photoSet))

    fun valid(photoSet: PhotoSet): Completable = when(photoSet.isValidForAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("Photoset validation failed before add"))
    }
}