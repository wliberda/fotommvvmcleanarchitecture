package domain.com.photoset.usecase

import domain.com.photoset.PhotoSet
import domain.com.photoset.repository.PhotoSetRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class RemovePhotoSetUseCase(val repository: PhotoSetRepository) {
    fun remove(photoSet: PhotoSet): Completable = repository.removePhotoSet(photoSet)
}