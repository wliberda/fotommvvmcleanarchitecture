package domain.com.photoset.usecase

import domain.com.photoset.repository.PhotoSetRepository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllPhotosetUseCase(val repository: PhotoSetRepository) {
    fun getAll() = repository.getAllPhotoSet()
}