package domain.com.photoset

import domain.com.location.Location
import domain.com.note.Note
import domain.com.photo.Photo
import domain.com.photosetmembership.PhotoSetMembership
import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class PhotoSet(val uuid: UUID,
                    val created: String?,
                    val changed: String?,
                    val author: User?,
                    var name: String? = null,
                    var description: Note? = null,
                    var location: Location? = null,
                    var photos: List<Photo>? = emptyList(),
                    var mainPhotoId: String? = null,
                    var url: String? = null,
                    var visibility: String? = null,
                    var listOfMembership: List<PhotoSetMembership>? = emptyList(),
                    var photoCount: Int? = 0,
                    var memberCount: Int? = 0,
                    var flagTypeCount: Int? = 0,
                    var active: Boolean = false
) {

    fun isValidForAdd() = name != null
    fun isValidForEdit() = name != null

}