package domain.com.note

import com.sun.org.apache.xpath.internal.operations.Bool
import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Note(val uuid: UUID?,
                val created: String?,
                val changed: String?,
                val author: User?,
                val sync: Boolean = false,
                val active: Boolean,
                val text: String?,
                val filePath: String?) {

    fun isValidForAdd() = uuid != null
    fun isValidForEdit() = uuid != null
    fun isValidForRemove() = uuid != null

}