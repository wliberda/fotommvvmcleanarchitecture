package domain.com.note.usecase

import domain.com.note.repository.NoteRepository
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindNoteUseCase(val repository: NoteRepository) {
    fun findByUUID(uuid: UUID) = repository.findNoteByUUID(uuid)

}