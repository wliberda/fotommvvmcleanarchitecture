package domain.com.note.usecase

import domain.com.note.repository.NoteRepository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllNoteUseCase(val repository: NoteRepository) {
    fun getAllNote() = repository.getAllNotes()
}