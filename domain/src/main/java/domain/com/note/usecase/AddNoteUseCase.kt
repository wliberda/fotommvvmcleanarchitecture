package domain.com.note.usecase

import domain.com.note.Note
import domain.com.note.repository.NoteRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddNoteUseCase(val repository: NoteRepository) {
    fun add(note: Note): Completable = valid(note).andThen(repository.addOrEditNote(note))


    private fun valid(note: Note): Completable = when(note.isValidForAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("note validation failed before add"))
    }
}