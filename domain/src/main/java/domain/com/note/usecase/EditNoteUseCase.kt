package domain.com.note.usecase

import domain.com.note.Note
import domain.com.note.repository.NoteRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditNoteUseCase(val repository: NoteRepository) {
    fun edit(note: Note): Completable = valid(note).andThen(repository.addOrEditNote(note))
    fun valid(note: Note): Completable = when (note.isValidForEdit()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("Note validation failed before edit"))
    }
}