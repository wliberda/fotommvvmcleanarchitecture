package domain.com.note.usecase

import domain.com.note.Note
import domain.com.note.repository.NoteRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class RemoveNoteUseCase(val repository: NoteRepository) {

    fun remove(note: Note) = valid(note).andThen(repository.removeNote(note))
    fun valid(note: Note) = when(note.isValidForRemove()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("Note validation failed before remove"))
    }
}