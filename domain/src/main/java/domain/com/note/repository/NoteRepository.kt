package domain.com.note.repository

import domain.com.note.Note
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface NoteRepository {
    fun findNoteByUUID(uuid: UUID) : Maybe<Note>
    fun addOrEditNote(note: Note): Completable
    fun removeNote(note: Note): Completable
    fun getAllNotes(): Single<List<Note>>
}