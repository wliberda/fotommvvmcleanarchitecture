package domain.com.note.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface NoteModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}