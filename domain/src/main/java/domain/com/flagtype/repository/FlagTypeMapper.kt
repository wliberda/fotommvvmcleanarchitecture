package domain.com.flagtype.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface FlagTypeMapper<E,M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}