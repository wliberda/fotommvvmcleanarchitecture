package domain.com.flagtype.repository

import domain.com.flagtype.FlagType
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface FlagTypeRepository {
    fun addOrEditFlagType(flagType: FlagType): Completable
    fun removeFlagType(flagType: FlagType): Completable
    fun findFlagTypeByUUID(uuid: UUID): Maybe<FlagType>
    fun getAllFlagType(): Single<List<FlagType>>
}