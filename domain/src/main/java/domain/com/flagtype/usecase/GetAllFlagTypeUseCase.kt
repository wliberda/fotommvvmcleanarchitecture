package domain.com.flagtype.usecase

import domain.com.flagtype.FlagType
import domain.com.flagtype.repository.FlagTypeRepository
import io.reactivex.Single

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllFlagTypeUseCase(val repository: FlagTypeRepository) {
    fun getAllFlagType(): Single<List<FlagType>> = repository.getAllFlagType()
}