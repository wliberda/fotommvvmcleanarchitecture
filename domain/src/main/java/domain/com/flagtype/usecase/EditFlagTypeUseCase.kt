package domain.com.flagtype.usecase

import domain.com.flagtype.FlagType
import domain.com.flagtype.repository.FlagTypeRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditFlagTypeUseCase(val repository: FlagTypeRepository) {

    fun editFlagType(flagType: FlagType): Completable = vaild(flagType).andThen(repository.addOrEditFlagType(flagType))

    fun vaild(flagType: FlagType): Completable =
            when (flagType.isValidForEdit()) {
                true -> Completable.complete()
                false -> Completable.error(IllegalArgumentException("flagtype validation failed before edit"))
            }
}