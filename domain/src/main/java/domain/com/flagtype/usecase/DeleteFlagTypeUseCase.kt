package domain.com.flagtype.usecase

import domain.com.flagtype.FlagType
import domain.com.flagtype.repository.FlagTypeRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class DeleteFlagTypeUseCase(val repository: FlagTypeRepository) {
    fun delete(flagType: FlagType) = valid(flagType).andThen(repository.removeFlagType(flagType))
    fun valid(flagType: FlagType): Completable = when(flagType.isValidForRemove()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("flagtype validation failed before remove"))
    }
}