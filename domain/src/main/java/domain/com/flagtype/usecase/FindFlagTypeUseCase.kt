package domain.com.flagtype.usecase

import domain.com.flagtype.FlagType
import domain.com.flagtype.repository.FlagTypeRepository
import io.reactivex.Maybe
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindFlagTypeUseCase(val repository: FlagTypeRepository) {
    fun findByUUID(uuid: UUID) : Maybe<FlagType> = repository.findFlagTypeByUUID(uuid)
}