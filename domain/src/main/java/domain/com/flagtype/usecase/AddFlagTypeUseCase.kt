package domain.com.flagtype.usecase

import domain.com.flagtype.FlagType
import domain.com.flagtype.repository.FlagTypeRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddFlagTypeUseCase(private val repository: FlagTypeRepository) {
    fun addFlagType(flagType: FlagType): Completable = valid(flagType).andThen(repository.addOrEditFlagType(flagType))

    fun valid(flagType: FlagType): Completable =
            when (flagType.isValidForAdd()) {
                true -> Completable.complete()
                false -> Completable.error(IllegalArgumentException("Flagtype validation failed before adding"))
            }
}