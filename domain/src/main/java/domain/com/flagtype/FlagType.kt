package domain.com.flagtype

import domain.com.flag.Flag
import domain.com.note.Note
import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class FlagType(val uuid: UUID?,
                    val created: String?,
                    val changed: String?,
                    val author: User?,
                    var title: String? = null,
                    var purpose: String? = null,
                    var notes: List<Note>? = emptyList(),
                    var color: String? = null,
                    var flags: List<Flag>? = emptyList(),
                    var isPresumed: Boolean = false,
                    var flagCount: Int = 0) {

    fun isValidForAdd() = uuid != null && title != null && color != null
    fun isValidForEdit() = uuid != null && title  != null && color != null
    fun isValidForRemove() = uuid != null
}