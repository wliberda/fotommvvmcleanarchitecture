package domain.com.todo.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface TodoModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}