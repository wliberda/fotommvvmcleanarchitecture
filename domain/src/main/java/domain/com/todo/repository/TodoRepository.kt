package domain.com.todo.repository

import domain.com.todo.Todo
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface TodoRepository {
    fun addOrEditTodo(todo: Todo): Completable
    fun removeTodo(todo: Todo): Completable
    fun findTodo(uuid: UUID): Maybe<Todo>
    fun getAllTodo(): Single<List<Todo>>
}