package domain.com.todo.usecase

import domain.com.todo.Todo
import domain.com.todo.repository.TodoRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class RemoveTodoUseCase(val repository: TodoRepository) {
    fun remove(todo: Todo): Completable = valid(todo).andThen(repository.removeTodo(todo))

    fun valid(todo: Todo): Completable = when(todo.isValidToRemove()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("todo validation failed before remove"))
    }
}