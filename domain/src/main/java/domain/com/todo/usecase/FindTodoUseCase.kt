package domain.com.todo.usecase

import domain.com.todo.Todo
import domain.com.todo.repository.TodoRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindTodoUseCase(val repository: TodoRepository) {
    fun findByUUID(uuid: UUID): Maybe<Todo> = repository.findTodo(uuid)


}