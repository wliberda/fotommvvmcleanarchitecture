package domain.com.todo.usecase

import domain.com.todo.Todo
import domain.com.todo.repository.TodoRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditTodoUseCase(val repository: TodoRepository) {
    fun edit(todo: Todo):Completable = Companion.valid(todo).andThen(repository.addOrEditTodo(todo))

    companion object {
        fun valid(todo: Todo) = when(todo.isValidToEdit()) {
            true -> Completable.complete()
            false -> Completable.error(IllegalArgumentException("todo validation failed before edit"))
        }!!
    }
}