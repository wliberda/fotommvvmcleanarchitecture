package domain.com.todo.usecase

import domain.com.todo.Todo
import domain.com.todo.repository.TodoRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddTodoUseCase(val repository: TodoRepository) {
    fun add(todo: Todo) = valid(todo).andThen(repository.addOrEditTodo(todo))

    private fun valid(todo: Todo) = when(todo.isValidToAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("todo validation failed before add"))
    }!!
}