package domain.com.todo

import domain.com.flag.Flag
import domain.com.photo.Photo
import domain.com.photoset.PhotoSet
import domain.com.photosetmembership.PhotoSetMembership
import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Todo(val uuid: UUID?,
                val created: String?,
                val changed: String?,
                val author: User?,
                var completedBy: User? = null,
                var assignedTo: List<PhotoSetMembership>? = emptyList(),
                var photo: Photo? = null,
                var flag: Flag? = null,
                var photoSet: PhotoSet? = null,
                var timeSpent: Int = 0,
                var listOfPhotos: List<Photo>? = emptyList(),
                var teamUUID: String? = null) {

    fun isValidToAdd() = uuid != null
    fun isValidToEdit() = uuid != null
    fun isValidToRemove() = uuid != null
}