package domain.com.location.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface LocationModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}