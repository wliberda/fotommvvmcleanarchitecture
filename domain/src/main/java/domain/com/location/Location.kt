package domain.com.location

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Location(var lat: Double? = 0.toDouble(),
                    var lon: Double? = 0.toDouble()) {
}