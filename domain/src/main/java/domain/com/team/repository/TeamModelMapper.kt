package domain.com.team.repository

/**
 * Created by wojciechliberda on 21/01/2018.
 */
interface TeamModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}