package domain.com.team.repository

import domain.com.team.Team
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 21/01/2018.
 */
interface TeamRepository {
    fun addOrEditTeam(team: Team): Completable
    fun removeTeam(team: Team): Completable
    fun findByUUID(uuid: UUID): Maybe<Team>
    fun getAllTeam(): Single<List<Team>>
    fun getTeamForUser(uuid: UUID): Single<List<Team>>
}