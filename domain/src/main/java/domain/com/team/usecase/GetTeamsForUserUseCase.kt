package domain.com.team.usecase

import domain.com.base.SingleUseCase
import domain.com.team.Team
import domain.com.team.repository.TeamRepository
import io.reactivex.Completable
import io.reactivex.Single
import java.security.InvalidParameterException
import java.util.*

/**
 * Created by wojciechliberda on 19/03/2018.
 */
open class GetTeamsForUserUseCase(val teamRepository: TeamRepository) : SingleUseCase<List<Team>, UUID>() {

    override fun buildUseCaseSingle(params: UUID?): Single<List<Team>> =
            valid(params)
                    .andThen(teamRepository.getTeamForUser(params!!))


    private fun valid(uuid: UUID?): Completable =
            if (uuid == null) Completable.error(InvalidParameterException())
            else Completable.complete()


}