package domain.com.team.usecase

import domain.com.team.Team
import domain.com.team.repository.TeamRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class AddTeamUseCase(val repository: TeamRepository) {
    fun add(team: Team): Completable = valid(team).andThen(repository.addOrEditTeam(team))

    fun valid(team: Team): Completable = when(team.isValidForAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("team validation failed before add"))
    }
}