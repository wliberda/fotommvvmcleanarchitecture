package domain.com.team.usecase

import domain.com.team.Team
import domain.com.team.repository.TeamRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class RemoveTeamUseCase(val repository: TeamRepository) {
    fun remove(team: Team): Completable = valid(team).andThen(repository.removeTeam(team))

    fun valid(team: Team): Completable = when(team.isValidForRemove()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("team validation failed before remove"))
    }
}