package domain.com.team

import domain.com.media.Media
import java.util.*

/**
 * Created by wojciechliberda on 21/01/2018.
 */
data class Team(var uuid: UUID? = null,
                var created: String? = null,
                var changed: String? = null,
                var sync: Boolean = false,
//var listOfFlagTypes: RealmList<FlagTypeRealm>? = null
//var listOfMemberships: RealmList<UserMembershipRealm> = RealmList()
                var teamName: String? = null,
                var isVerified: Boolean = false,
                var teamUrl: String? = null,
                var purpose: String? = null,
//                var domains: List<String> = emptyList(),
//                var avatarUrl: String? = null,
                var surveyCount: Int = 0,
                var flagTypeCount: Int = 0
                ) {

    fun isValidForAdd() = teamName != null && uuid != null
    fun isValidForRemove() = uuid != null

}