package domain.com.user.repository

import domain.com.team.Team
import domain.com.user.User
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface UserRepository {

    fun fetchAndSaveUser(uuid: UUID): Completable
    fun addOrEditUser(user: User): Completable
    fun deleteUser(user: User): Completable
    fun findUserByUUID(uuid: UUID): Maybe<User>
    fun findAllUser(): Single<List<User>>
}