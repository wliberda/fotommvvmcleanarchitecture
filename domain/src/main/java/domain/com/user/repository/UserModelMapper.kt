package domain.com.user.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface UserModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}