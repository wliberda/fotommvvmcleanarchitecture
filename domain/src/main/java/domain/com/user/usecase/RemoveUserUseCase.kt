package domain.com.user.usecase

import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class RemoveUserUseCase(val repository: UserRepository) {
    fun remove(user: User) = valid(user).andThen(repository.deleteUser(user))

    fun valid(user: User) = when(user.isValidForRemove()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("User validation failed before remove"))
    }
}