package domain.com.user.usecase

import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditUserUseCase(val repository: UserRepository) {
    fun edit(user: User) = valid(user).andThen(repository.addOrEditUser(user))

    fun valid(user: User) = when(user.isValidForEdit()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("User validation failed before edit"))
    }
}