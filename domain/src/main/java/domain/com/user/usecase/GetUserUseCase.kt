package domain.com.user.usecase

import domain.com.base.MaybeUseCase
import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Maybe
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetUserUseCase(private val repository: UserRepository) : MaybeUseCase<User, UUID>() {
    override fun buildMaybeUseCase(param: UUID?): Maybe<User> = repository.findUserByUUID(param!!)


    private fun findByUUID(uuid: UUID) = repository.findUserByUUID(uuid)

}