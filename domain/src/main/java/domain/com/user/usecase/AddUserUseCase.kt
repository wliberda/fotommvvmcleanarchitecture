package domain.com.user.usecase

import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddUserUseCase(val repository: UserRepository) {
    fun add(user: User): Completable = valid(user).andThen(repository.addOrEditUser(user))
    fun valid(user: User): Completable = when(user.isValidForAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("user validation failed before add"))
    }
}