package domain.com.user

import com.sun.org.apache.xpath.internal.operations.Bool
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class User(val uuid: UUID? = null,
                val changed: String? = null,
                val created: String? = null,
                val sync: Boolean? = false,
                val active: Boolean? = false,
                var firstName: String? = null,
                var lastName: String? = null,
                var email: String? = null,
                val text: String? = null,
                var userName: String? = null,
                var occupation: String? = null,
                val attachment: String? = null) {

    fun isValidForAdd() = uuid != null && firstName != null && email != null && userName != null
    fun isValidForEdit() = uuid != null && firstName != null && email != null && userName != null
    fun isValidForRemove() = uuid != null

}