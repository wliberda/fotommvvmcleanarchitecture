package domain.com.photo

import domain.com.flag.Flag
import domain.com.location.Location
import domain.com.media.Media
import domain.com.note.Note
import domain.com.photoset.PhotoSet
import domain.com.user.User
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Photo(val uuid: UUID,
                 val created: String?,
                 val changed: String?,
                 val author: User?,
                 val description: Note? = null,
                 var notes: List<Note>? = emptyList(),
                 var orientation: Int = -1,
                 var active: Boolean = false,
                 var media: Media? = null,
                 var name: String? = null,
                 var location: Location? = null,
                 var flags: List<Flag>? = emptyList<Flag>(),
                 var flagCount: Int? = 0,
                 var photoSet: PhotoSet? = null) {
    fun isValidForAdd() = media != null
    fun isValidForEdit() = media != null


}