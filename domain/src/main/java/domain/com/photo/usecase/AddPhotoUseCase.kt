package domain.com.photo.usecase

import domain.com.photo.Photo
import domain.com.photo.repository.PhotoRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddPhotoUseCase(val repository: PhotoRepository) {
    fun add(photo: Photo): Completable = valid(photo).andThen(repository.addOrEditPhoto(photo))

    fun valid(photo: Photo): Completable = when(photo.isValidForAdd()) {
        true -> Completable.complete()
        false -> Completable.error(IllegalArgumentException("Photo validation failed before add"))
    }
}