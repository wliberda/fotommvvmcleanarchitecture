package domain.com.photo.usecase

import domain.com.photo.repository.PhotoRepository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllPhotoFromUseCase(val repository: PhotoRepository) {
    fun getAllPhoto() = repository.getAllPhotos()
}