package domain.com.photo.usecase

import domain.com.photo.Photo
import domain.com.photo.repository.PhotoRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class RemovePhotoUseCase(val repository: PhotoRepository) {
    fun remove(photo: Photo): Completable = repository.removePhoto(photo)

}