package domain.com.photo.usecase

import domain.com.photo.repository.PhotoRepository
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindPhotoUseCase(val repository: PhotoRepository) {
    fun findByUUID(uuid: UUID) = repository.findPhotoByUUId(uuid)
}