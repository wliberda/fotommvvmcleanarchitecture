package domain.com.photo.repository

import domain.com.photo.Photo
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoRepository {
    fun addOrEditPhoto(photo: Photo): Completable
    fun removePhoto(photo: Photo): Completable
    fun findPhotoByUUId(uuid: UUID): Maybe<Photo>
    fun getAllPhotos(): Single<List<Photo>>
}