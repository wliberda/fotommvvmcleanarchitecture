package domain.com.photo.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface PhotoModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}