package domain.com.flag.usecase

import domain.com.flag.Flag
import domain.com.flag.repository.FlagRepository
import io.reactivex.Maybe
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class FindFlagUseCase(private val flagRepository: FlagRepository) {
    fun findFlagByUUID(uuid: UUID) : Maybe<Flag> = flagRepository.findById(uuid)
}