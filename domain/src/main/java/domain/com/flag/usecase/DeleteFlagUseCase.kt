package domain.com.flag.usecase

import domain.com.flag.Flag
import domain.com.flag.repository.FlagRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class DeleteFlagUseCase(private val flagRepository: FlagRepository) {

    fun remove(flag: Flag) : Completable = validate(flag).andThen(flagRepository.delete(flag))

    private fun validate(flag: Flag): Completable =
            when(flag.isValidForRemove()) {
                true -> Completable.complete()
                false -> Completable.error(IllegalArgumentException("Flag validation failed for remove"))
            }
}