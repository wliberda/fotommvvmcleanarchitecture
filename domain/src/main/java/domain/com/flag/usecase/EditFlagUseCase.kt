package domain.com.flag.usecase

import domain.com.flag.Flag
import domain.com.flag.repository.FlagRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class EditFlagUseCase(private val flagRepository: FlagRepository) {

    fun edit(flag: Flag): Completable = valide(flag).andThen(flagRepository.insertOrUpdate(flag))

    private fun valide(flag: Flag) : Completable =
            when(flag.isValidForEdit()) {
                true -> Completable.complete()
                false -> Completable.error(IllegalArgumentException("Flag validation failed when edit"))
            }
}