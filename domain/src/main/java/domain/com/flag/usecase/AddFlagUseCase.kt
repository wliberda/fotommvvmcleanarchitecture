package domain.com.flag.usecase

import domain.com.flag.Flag
import domain.com.flag.repository.FlagRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddFlagUseCase(private val flagRepository: FlagRepository) {

    fun add(flag: Flag): Completable = validate(flag).andThen(flagRepository.insertOrUpdate(flag))

    private fun validate(flag: Flag):Completable =
            when(flag.isValidForAdd()) {
                true -> Completable.complete()
                false -> Completable.error(IllegalArgumentException("flag validation failed before add"))
            }
}