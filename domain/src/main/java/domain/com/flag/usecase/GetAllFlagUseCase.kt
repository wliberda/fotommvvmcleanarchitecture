package domain.com.flag.usecase

import domain.com.flag.Flag
import domain.com.flag.repository.FlagRepository
import io.reactivex.Single

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllFlagUseCase(private val flagRepository: FlagRepository) {
    fun getAllFlags() : Single<List<Flag>> = flagRepository.getAllFlags()
}