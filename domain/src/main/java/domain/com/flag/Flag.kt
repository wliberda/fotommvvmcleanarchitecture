package domain.com.flag

import domain.com.flagtype.FlagType
import domain.com.media.Media
import domain.com.note.Note
import domain.com.photo.Photo
import domain.com.todo.Todo
import domain.com.user.User
import java.util.*


/**
 * Created by wojciechliberda on 20/01/2018.
 */
data class Flag(val uuid: UUID?,
                val created: String?,
                val changed: String?,
                val author: User?,
                var notes: List<Note>? = emptyList(),
                var sync: Boolean = false,
                var todo: Todo? = null,
                var active: Boolean = false,
                var top: Double? = null,
                var left: Double? = null,
                var flagType: FlagType? = null,
                var audioRecording: List<Media>? = emptyList(),
                var detailPhotos: List<Media>? = emptyList(),
                var photo: Photo? = null) {

    fun isValidForAdd() = uuid != null && flagType != null && left != null && top != null
    fun isValidForEdit() = uuid != null && flagType != null && left != null && top != null
    fun isValidForRemove() = uuid != null


}