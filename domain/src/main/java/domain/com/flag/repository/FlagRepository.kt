package domain.com.flag.repository

import domain.com.flag.Flag
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface FlagRepository {
    fun insertOrUpdate(flag: Flag): Completable
    fun delete(flag: Flag): Completable
    fun findById(uuid: UUID): Maybe<Flag>
    fun getAllFlags(): Single<List<Flag>>
}