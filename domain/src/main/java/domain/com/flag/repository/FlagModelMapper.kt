package domain.com.flag.repository

/**
 * Created by wojciechliberda on 20/01/2018.
 */
interface FlagModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E

}