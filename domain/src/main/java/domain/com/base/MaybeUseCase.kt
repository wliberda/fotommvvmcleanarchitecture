package domain.com.base

import io.reactivex.Maybe
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableMaybeObserver

/**
 * Created by wojciechliberda on 09/02/2018.
 */
abstract class MaybeUseCase<T, in Param> {

    abstract internal fun buildMaybeUseCase(param: Param?) : Maybe<T>

    private var disposable = Disposables.empty()

    fun execute(observer: DisposableMaybeObserver<T>, param: Param? = null) {
        disposable = buildMaybeUseCase(param).subscribeWith(observer)
    }

    fun dispose() {
        if(!disposable.isDisposed)
            disposable.dispose()
    }
}