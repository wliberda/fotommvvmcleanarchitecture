package domain.com.base.exceptions

/**
 * Created by wojciechliberda on 16/02/2018.
 */
abstract class DefaultErrorBundle(val exception: Exception)

