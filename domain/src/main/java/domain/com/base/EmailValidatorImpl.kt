package domain.com.base

import java.util.regex.Pattern

/**
 * Created by wojciechliberda on 18/02/2018.
 */

class EmailValidatorImpl: EmailValidator{

    override fun validate(emailStr: String): Boolean =
        VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr).run {
            this.find()
        }

}