package domain.com.base

import io.reactivex.Completable
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableCompletableObserver

/**
 * Created by wojciechliberda on 08/02/2018.
 */
abstract class CompletableUseCase<in Params> {
    abstract fun buildUseCaseComplete(params: Params?): Completable

    private var disposable = Disposables.empty()

    open fun execute(observer: DisposableCompletableObserver, params: Params? = null) {
        disposable = buildUseCaseComplete(params).subscribeWith(observer)
    }

    fun dispose() {
        if (!disposable.isDisposed) disposable.dispose()
    }

}