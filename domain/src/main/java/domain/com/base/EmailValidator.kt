package domain.com.base

import java.util.regex.Pattern

/**
 * Created by wojciechliberda on 18/02/2018.
 */
interface EmailValidator {
    val VALID_EMAIL_ADDRESS_REGEX: Pattern
        get() = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)!!
    fun validate(emailStr: String): Boolean
}