package domain.com.base

import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber

/**
 * Created by wojciechliberda on 09/02/2018.
 */
abstract class SingleUseCase<T, in Params> {

    var disposable = Disposables.empty()

    internal abstract fun buildUseCaseSingle(params: Params? = null): Single<T>

    open fun execute(observer: DisposableSingleObserver<T>, params: Params) {
        disposable = buildUseCaseSingle(params).subscribeWith(observer)
    }

    fun dispose() {
        if (!disposable.isDisposed)
            disposable.dispose()
    }

}