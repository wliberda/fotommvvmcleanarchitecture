package domain.com.login.repository

import domain.com.login.Login
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

/**
 * Created by wojciechliberda on 07/02/2018.
 */
interface LoginRepository {
    fun logIn(login: Login): Maybe<UUID>
}