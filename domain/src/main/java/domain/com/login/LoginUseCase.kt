package domain.com.login

import domain.com.base.CompletableUseCase
import domain.com.base.EmailValidator
import domain.com.base.exceptions.EmailValidationException
import domain.com.login.exception.PasswordEmptyOrShortException
import domain.com.login.repository.LoginRepository
import domain.com.user.repository.UserRepository
import io.reactivex.Completable

/**
 * Created by wojciechliberda on 07/02/2018.
 */
open class LoginUseCase(private val loginRepository: LoginRepository,
                        private val userRepository: UserRepository,
                        private val emailValidator: EmailValidator) : CompletableUseCase<Login>() {


    override fun buildUseCaseComplete(params: Login?): Completable =
            valid(params!!)
                    .andThen(loginRepository.logIn(params))
                    .flatMapCompletable{ userRepository.fetchAndSaveUser(it) }



    private fun valid(login: Login): Completable = when {
        login.userName == null || !emailValidator.validate(login.userName) || login.userName == "" -> Completable.error(EmailValidationException())
        login.password == null || login.password == "" || login.password.length < 6 -> Completable.error(PasswordEmptyOrShortException())
        else -> Completable.complete()
    }
}