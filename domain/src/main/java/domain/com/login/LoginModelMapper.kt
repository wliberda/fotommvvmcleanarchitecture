package domain.com.login

/**
 * Created by wojciechliberda on 08/02/2018.
 */
interface LoginModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}