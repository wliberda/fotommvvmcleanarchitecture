package domain.com.login

/**
 * Created by wojciechliberda on 07/02/2018.
 */
data class Login(val grantType: String?,
                 val userName: String?,
                 val password: String?) {
}

