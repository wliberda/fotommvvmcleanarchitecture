package domain.com.base

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

/**
 * Created by wojciechliberda on 09/02/2018.
 */
class SingleUseCaseTest{
    lateinit var testSubject: SingleUseCaseTestClass
    lateinit var testObserver: TestDisposableSingleObserver<Any>
    @Before
    fun setUp() {
        testSubject = SingleUseCaseTestClass()
        testObserver = TestDisposableSingleObserver()
    }

    @Test
    fun shouldNotBeNull() {
        assertNotNull(testSubject)
    }

    @Test
    fun shouldBeDisposeAfterDispose() {
        testSubject.execute(testObserver, Params())
        testSubject.dispose()
        assertTrue(testObserver.isDisposed)
    }

    @Test
    fun test() {
        testSubject.execute(testObserver, Params())
        assert(testObserver.valuesCount == 0)
    }

    class TestDisposableSingleObserver<T> : DisposableSingleObserver<T>() {
        val valuesCount = 0
        override fun onSuccess(t: T) {

        }

        override fun onError(e: Throwable) {

        }
    }

    class SingleUseCaseTestClass :  SingleUseCase<Any, Params>() {
        override fun buildUseCaseSingle(params: Params?): Single<Any> {
            return Observable.empty<Any>().singleOrError()
        }
    }

    companion object {
        class Params {

        }
    }
}
