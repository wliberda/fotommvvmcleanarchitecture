package domain.com.base

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by wojciechliberda on 18/02/2018.
 */
class EmailValidatorImplTest {

    lateinit var testSubject: EmailValidatorImpl

    @Before
    fun setUp() {
        testSubject = EmailValidatorImpl()
    }

    @Test
    fun `subject coulnd be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `when get Valid email should return true`() {
        val email = "wliberda@omnis.pl"
        assertTrue(testSubject.validate(email))
    }

    @Test
    fun `when get NotValid email should return false`() {
        val string = "qweqweqwe"
        assertFalse(testSubject.validate(string))
    }



}