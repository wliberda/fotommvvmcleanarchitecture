package domain.com.login

import com.nhaarman.mockito_kotlin.*
import domain.com.base.EmailValidator
import domain.com.base.exceptions.EmailValidationException
import domain.com.login.exception.PasswordEmptyOrShortException
import domain.com.login.repository.LoginRepository
import domain.com.user.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

/**
 * Created by wojciechliberda on 09/02/2018.
 */
class LoginUseCaseTest {
    private val loginRepository: LoginRepository = mock()
    private lateinit var testSubject: LoginUseCase
    private val emailValidator: EmailValidator = mock()
    private val userRepository: UserRepository = mock()
    companion object {
        val VALID_EMAIL = "wliberda@omnis.pl"
        val NOTVALID_EMAIL = "qqqqqqe"
        val VALID_PASSWORD = "password"
        val NOTVALID_PASSWORD = "ddd"
        val UUID = java.util.UUID.randomUUID()
    }

    @Before
    fun setUp() {
        whenever(loginRepository.logIn(any())).thenReturn(Maybe.just(UUID))
        testSubject = LoginUseCase(loginRepository, userRepository, emailValidator)
    }

    @Test
    fun shouldNotBeNull() {
        assertNotNull(testSubject)
    }

    @Test
    fun `given Valid Login when login then return complete`() {
        whenever(emailValidator.validate(anyString())).thenReturn(true)
        whenever(userRepository.fetchAndSaveUser(any())).thenReturn(Completable.complete())
        val testObserver = testSubject.buildUseCaseComplete(getValidLogin()).test()
        verify(loginRepository, times(1)).logIn(any())
        verify(userRepository, times(1)).fetchAndSaveUser(any())
        verifyNoMoreInteractions(loginRepository)
        verifyNoMoreInteractions(userRepository)
        testObserver.assertComplete()
    }

   @Test
   fun `when empty or short password return exception`() {
       whenever(emailValidator.validate(anyString())).thenReturn(true)
       val testObserver = testSubject.buildUseCaseComplete(getLogin(VALID_EMAIL, NOTVALID_PASSWORD)).test()
       testObserver.assertError(PasswordEmptyOrShortException::class.java)
       verifyNoMoreInteractions(userRepository)
   }

    @Test
    fun `when email not valid return Exception`() {
        whenever(emailValidator.validate(anyString())).thenReturn(false)
        val testObserver = testSubject.buildUseCaseComplete(getLogin(NOTVALID_EMAIL, VALID_PASSWORD)).test()
        testObserver.assertError(EmailValidationException::class.java)
    }



    private fun getValidLogin() = Login("password", "wliberda@omnis.pl", "password")
    private fun getLogin(userName: String? = "",
                                 password: String? = "") = Login("password", userName, password)

}