package domain.com.user.usecase

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import domain.com.user.User
import domain.com.user.repository.UserRepository
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

import org.mockito.Mockito
import java.util.*
import io.reactivex.observers.DefaultObserver
import io.reactivex.observers.TestObserver
import io.reactivex.subscribers.TestSubscriber
import org.mockito.ArgumentCaptor
import org.mockito.Captor



/**
 * Created by wojciechliberda on 20/01/2018.
 */
class AddUserUseCaseTest {
    private val repository: UserRepository = mock()
    private lateinit var testSubject: AddUserUseCase

    companion object {
        val uuid = UUID.randomUUID()
        val created = "created"
        val changed = "changed"
        val firstName = "firstName"
        val lastName = "lastName"
        val userName = "userName"
        val email = "email@email.com"
        val active = true
        val sync = true
        val text = "text"
        val occupation = "occupation"
    }


    @Before
    fun setUp() {
        testSubject = AddUserUseCase(repository)
    }




    @Test
    fun shouldAddUser() {
        val user = getValidUser()
        val completable = Completable.complete()
        whenever(repository.addOrEditUser(user)).thenReturn(completable)
        val response = testSubject.add(user).test()
        response.hasSubscription()
        assert(response.hasSubscription())
    }
//
//    @Test
//    fun shouldThrowIllegalArgumentException() {
//        val user = User()
//        val completable = Completable.complete()
//        whenever(repository.addOrEditUser(user)).thenReturn(completable)
//        val response = testSubject.add(user)
//        assert(response == Completable.error(IllegalArgumentException()))
//        assertNotEquals(response, Completable.complete())
//
//    }


    private fun getValidUser(): User = User(uuid, changed, created, sync, active, firstName, lastName, email, text, userName, occupation)
}