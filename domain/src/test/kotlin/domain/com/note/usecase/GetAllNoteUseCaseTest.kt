package domain.com.note.usecase

import com.nhaarman.mockito_kotlin.*
import domain.com.note.Note
import domain.com.note.repository.NoteRepository
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by wojciechliberda on 20/01/2018.
 */
class GetAllNoteUseCaseTest {

    private val repository: NoteRepository = mock()
    private lateinit var testSubject: GetAllNoteUseCase
    @Before
    fun setUp() {
        testSubject = GetAllNoteUseCase(repository)
    }

    @Test
    fun shouldReturnListOfNotes(){
        val list = mutableListOf<Note>()
        list.add(Note(UUID.randomUUID(),null, null,null,true,true,"",""))
        val single = Single.just(list as List<Note>)
        whenever(repository.getAllNotes()).thenReturn(single)

        val a = testSubject.getAllNote()
        verify(repository, times(1)).getAllNotes()
        verifyNoMoreInteractions(repository)
        assertEquals(a, single)


    }

}