package com.fotommvvmcleanarchitecture.mapper

import com.fotommvvmcleanarchitecture.utils.DateUtils
import domain.com.user.User
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import java.util.*

/**
 * Created by wojciechliberda on 17/02/2018.
 */
class UserModelDataMapperTest {

    companion object {
        val uuid = UUID.randomUUID()
        val created = "created"
        val changed = "changed"
        val firstName = "firstName"
        val lastName = "lastName"
        val userName = "userName"
        val email = "email@email.com"
        val active = true
        val sync = true
        val text = "text"
        val occupation = "occupation"
    }


    lateinit var testSubject: UserModelDataMapper

    @Before
    fun setUp() {
        testSubject = UserModelDataMapper(mock(DateUtils::class.java))
    }

    @Test
    fun `subject couldn be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `should return usermodel with parameters`() {
        val user = getValidUser()
        val model = testSubject.transform(user)
        assertTrue(model.uuid == user.uuid)
//        assertTrue(model.changed == user.changed)
//        assertTrue(model.created == user.created)
//        assertTrue(model.sync == user.sync)
        assertTrue(model.userName == user.userName)
        assertTrue(model.firstName == user.firstName)
        assertTrue(model.lastName == user.lastName)
        assertTrue(model.occupation == user.occupation)
        assertTrue(model.email == user.email)
    }


    @Test
    fun `should return list of usermodels`() {
        val list = arrayListOf<User>(getValidUser(), getValidUser())
        val modelList = testSubject.transformCollection(list)
        assertTrue(list.size == modelList.size)
    }

    fun getValidUser() = User(uuid, changed, created, sync, active, firstName, lastName, email, text, userName, occupation)


}