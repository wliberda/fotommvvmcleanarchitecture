package com.fotommvvmcleanarchitecture.factory

import java.util.*
import java.util.concurrent.ThreadLocalRandom

class DataFactory {
    companion object Factory {

        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        fun randomInt() = ThreadLocalRandom.current().nextInt(0, 1000+1)
        fun randomString() = UUID.randomUUID().toString()
        fun randomUUID() = UUID.randomUUID()
    }
}