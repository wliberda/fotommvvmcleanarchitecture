package com.fotommvvmcleanarchitecture.ui.chooseteam

import com.fotommvvmcleanarchitecture.factory.DataFactory.Factory.randomInt
import com.fotommvvmcleanarchitecture.factory.DataFactory.Factory.randomString
import domain.com.team.Team
import java.util.*

class TeamFactory {
    companion object Factory {
        fun makeTeamList(count: Int) : List<Team> {
            val list = mutableListOf<Team>()
            repeat(count) {
                list.add(makeTeamModel())
            }
            return list
        }

        private fun makeTeamModel(): Team =
                Team(UUID.randomUUID(),
                        randomString(),
                        randomString(),
                        true,
                        randomString(),
                        true,
                        randomString(),
                        randomString(),
                        randomInt(),
                        randomInt()

                        )
    }
}