package com.fotommvvmcleanarchitecture.ui.chooseteam

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.fotommvvmcleanarchitecture.ui.chooseteam.TeamFactory.Factory.makeTeamList
import com.fotommvvmcleanarchitecture.vo.Response
import com.fotommvvmcleanarchitecture.vo.Status
import com.nhaarman.mockito_kotlin.*
import domain.com.team.Team
import domain.com.team.usecase.GetTeamsForUserUseCase
import io.reactivex.observers.DisposableSingleObserver
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ChooseTeamViewModelTest {
    lateinit var getTeamsForUserUseCase: GetTeamsForUserUseCase

    lateinit var testSubject: ChooseTeamViewModel

    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    lateinit var argumentCaptor: KArgumentCaptor<DisposableSingleObserver<List<Team>>>


    @Before
    fun setUp() {
        getTeamsForUserUseCase = mock()
        testSubject = ChooseTeamViewModel(getTeamsForUserUseCase)
        argumentCaptor = argumentCaptor()

    }


    @Test
    fun `subject cannot be null`() {
        assertNotNull(testSubject)
    }



    @Test
    fun `should Success`() {
        val obs: Observer<Response<List<Team>>> = mock()
        testSubject.teams.observeForever(obs)
        testSubject.getTeams()
        testSubject.teams.value!!.apply {
            assertTrue(data == null)
            assertTrue(error == null)
            assertTrue(status == Status.LOADING)
        }
        verify(getTeamsForUserUseCase).execute(argumentCaptor.capture(), any())
        val list = makeTeamList(5)
        argumentCaptor.firstValue.onSuccess(list)
        testSubject.teams.value!!.apply {
            assertTrue(data == list)
            assertTrue(error == null)
            assertTrue(status == Status.SUCCESS)
        }
        verifyNoMoreInteractions(getTeamsForUserUseCase)

    }

    private fun assertLoading(response: Response<List<Team>>) {
        response.apply {
            assertTrue(status == Status.LOADING)
            assertTrue(data == null)
            assertTrue(error == null)
        }
    }

}