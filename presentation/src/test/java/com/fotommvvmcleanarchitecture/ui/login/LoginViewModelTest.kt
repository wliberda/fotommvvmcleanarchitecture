package com.fotommvvmcleanarchitecture.ui.login

import android.arch.lifecycle.Observer
import com.fotommvvmcleanarchitecture.vo.Response
import domain.com.login.LoginUseCase
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.fotommvvmcleanarchitecture.vo.Status
import com.nhaarman.mockito_kotlin.*
import io.reactivex.observers.DisposableCompletableObserver
import org.junit.Rule



/**
 * Created by wojciechliberda on 18/02/2018.
 */
class LoginViewModelTest{
    lateinit var loginUseCase: LoginUseCase

    lateinit var testSubject: LoginViewModel

    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()
    lateinit var argumentCaptor: KArgumentCaptor<DisposableCompletableObserver>
    companion object {
        val VALID_LOGIN = "wliberda@omnis.pl"
        val VALID_PASSWORD = "password"
    }

    @Before
    fun setUp() {
        loginUseCase =  mock()
        testSubject = LoginViewModel(loginUseCase)
        argumentCaptor = argumentCaptor()

    }

    @Test
    fun `subject shouldnt be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `when usecase Complete set value as Response-Succes`() {
        val obs: Observer<Response<Boolean>> = mock()
        testSubject.isLoginSuccess.observeForever(obs)
        testSubject.btnLogInClicked(VALID_LOGIN, VALID_PASSWORD)
        testSubject.isLoginSuccess.value!!.apply {
            assertTrue(status == Status.LOADING)
            assertTrue(data == null)
            assertTrue(error == null)
        }
//        val captor = argumentCaptor<Response<Boolean>>()
//        verify(obs).onChanged(captor.capture())
//        assertLoading(captor.firstValue)
//        reset(obs)
        verify(loginUseCase).execute(argumentCaptor.capture(), any())
        argumentCaptor.firstValue.onComplete()
        testSubject.isLoginSuccess.value!!.apply {
            assertTrue(status == Status.SUCCESS)
            assertTrue(data == true)
            assertTrue(error == null)
        }
//        val captor1 = com.nhaarman.mockito_kotlin.argumentCaptor<Response<Boolean>>()
//        verify(obs).onChanged(captor1.capture())
//        assertSuccess(captor1.firstValue)
        verifyNoMoreInteractions(loginUseCase)
    }

    @Test
    fun `when exception occurs set LoginValue as Response-Error with exception`() {
        val obs: Observer<Response<Boolean>> = mock()
        testSubject.isLoginSuccess.observeForever(obs)
        testSubject.btnLogInClicked(VALID_LOGIN, VALID_PASSWORD)
        val captor = argumentCaptor<Response<Boolean>>()
        verify(obs).onChanged(captor.capture())
        assertLoading(captor.firstValue)
        reset(obs)
        verify(loginUseCase).execute(argumentCaptor.capture(), any())
        argumentCaptor.firstValue.onError(Throwable())
        val captor1 = argumentCaptor<Response<Boolean>>()
        verify(obs).onChanged(captor1.capture())
        assertException(captor1.firstValue)
        verifyNoMoreInteractions(loginUseCase)
        verifyNoMoreInteractions(obs)
    }


    private fun assertLoading(response: Response<Boolean>) {
        response.apply {
            assertTrue(status == Status.LOADING)
            assertTrue(data == null)
            assertTrue(error == null)
        }
    }

    private fun assertSuccess(response: Response<Boolean>) {
        response.apply {
            assertTrue(status == Status.SUCCESS)
            assertTrue(data == true)
            assertTrue(error == null)
        }
    }


    private fun assertException(response: Response<Boolean>) {
        response.apply {
            assertTrue(status == Status.ERROR)
            assertTrue(data == null)
            assertTrue(error != null)
        }
    }

}