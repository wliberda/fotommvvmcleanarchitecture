package com.fotommvvmcleanarchitecture.ui.landing

import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.*
import android.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.rules.TestRule
import org.junit.Rule




/**
 * Created by wojciechliberda on 21/01/2018.
 */
class LandingViewModelTest {

     companion object {
         val uuid = UUID.randomUUID()
     }


    lateinit var testSubject: LandingViewModel

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        testSubject = LandingViewModel()
    }


    @Test
    fun subjectShouldNotBeNull() {
        assertNotNull(testSubject)
    }

    @Test
    fun `should open login activity when button clicked`() {
        val obs: Observer<Boolean> = mock()
        testSubject.btnLogInClicked()
        testSubject.isSignInButtonEnable.observeForever(obs)
        verify(obs).onChanged(true)
    }

}