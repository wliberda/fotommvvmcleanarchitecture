package com.fotommvvmcleanarchitecture.vo

/**
 * Created by wojciechliberda on 22/02/2018.
 */
sealed class Resource<T> {
    class Loading<T> : Resource<T>()
    class Success<T>(val data: T) : Resource<T>()
    class Error<T>(val exception: Throwable) : Resource<T>()
}