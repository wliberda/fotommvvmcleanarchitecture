package com.fotommvvmcleanarchitecture.vo


/**
 * Created by wojciechliberda on 18/02/2018.
 */
class Response<out T> private constructor(val status: Status, val  data: T?, val error: Throwable?) {
    companion object {


        fun <T> success(data: T?): Response<T> =
                Response(Status.SUCCESS, data, null)


        fun <T> error(throwable: Throwable): Response<T> =
                Response(Status.ERROR, null, throwable)

        fun <T> loading(): Response<T> =
                Response(Status.LOADING, null, null)

    }
}