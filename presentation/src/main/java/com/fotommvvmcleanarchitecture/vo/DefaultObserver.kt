package com.fotommvvmcleanarchitecture.vo

import android.arch.lifecycle.Observer
import android.widget.Toast
import com.network.NetworkException
import android.content.Context
import com.fotommvvmcleanarchitecture.R

/**
 * Created by wojciechliberda on 18/02/2018.
 */
abstract class DefaultObserver<T: Response<V>, V> (val appContext: Context) : Observer<T> {
    override fun onChanged(t: T?) {
        when(t?.error) {
            is NetworkException -> Toast.makeText(appContext, appContext.resources.getString(R.string.error_network_conntection), Toast.LENGTH_LONG).show()
        }
    }
}