package com.fotommvvmcleanarchitecture.vo

/**
 * Created by wojciechliberda on 18/02/2018.
 */
enum class Status {
        SUCCESS,
        ERROR,
        LOADING
}