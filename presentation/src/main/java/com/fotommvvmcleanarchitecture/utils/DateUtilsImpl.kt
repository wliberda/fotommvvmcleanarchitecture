package com.fotommvvmcleanarchitecture.utils

import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

/**
 * Created by wojciechliberda on 18/02/2018.
 */
class DateUtilsImpl : DateUtils {
    override fun parseToLocalDateTimeWithDateFormat(stringDate: String): LocalDateTime =
        LocalDateTime.parse(stringDate, DateTimeFormatter.ofPattern(DATE_FORMAT))

    override fun changeFormatToYearTime(localDateTime: LocalDateTime): String =
            localDateTime.format(DateTimeFormatter.ofPattern(YEAR_TIME_FORMAT))


}