package com.fotommvvmcleanarchitecture.utils

import org.threeten.bp.LocalDateTime
import java.util.*

/**
 * Created by wojciechliberda on 18/02/2018.
 */
interface DateUtils {

    val DATE_FORMAT: String
        get() = "yyyy-MM-dd'T'HH:mm:ssZ"

    val YEAR_TIME_FORMAT: String
        get() = "YYYY-MM-dd hh:mm"

    fun parseToLocalDateTimeWithDateFormat(stringDate: String): LocalDateTime

    fun changeFormatToYearTime(localDateTime: LocalDateTime): String


}