package com.fotommvvmcleanarchitecture.utils

import android.support.annotation.StringRes

        /**
 * Created by wojciechliberda on 18/02/2018.
 */
typealias SnackFinishedListener = () -> Unit
typealias SnackActionListener = () -> Unit

/**
 * Representation of [Snackbar] data that can be exposed by a view model, observed, and bound to
 * the current UI
 */
sealed class SnackData {
}

/**
 * Indicates that a [Snackbar] should be hidden
 */
object NoSnack : SnackData()


/**
 * Indicates a [Snackbar] that should not be automatically dismissed and is shown
 * with [Snackbar.LENGTH_INDEFINITE]
 */
open class StickySnack(
        @StringRes val msgId:Int, val dismissListener: SnackFinishedListener = {},
        @StringRes val actionMsgId:Int = 0, val actionListener: SnackFinishedListener = {})  : SnackData()

/**
 * Indicates a [Snackbar] that should be shown with [Snackbar.LENGTH_SHORT]
 */
data class ShortSnack(@StringRes val msgId:Int, val dismissListener: SnackFinishedListener = {},
                      @StringRes val actionMsgId:Int = 0, val actionListener: SnackFinishedListener = {})  : SnackData()

/**
 * Indicates a [Snackbar] that should be shown with [Snackbar.LENGTH_LONG]
 */
data class LongSnack(@StringRes val msgId:Int, val dismissListener: SnackFinishedListener = {},
                     @StringRes val actionMsgId:Int = 0, val actionListener: SnackFinishedListener = {})  : SnackData()