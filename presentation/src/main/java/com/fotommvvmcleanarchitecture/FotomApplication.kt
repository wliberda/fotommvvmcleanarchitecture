package com.fotommvvmcleanarchitecture

import android.app.Activity
import android.app.Application
import com.fotommvvmcleanarchitecture.di.AppComponent
import com.fotommvvmcleanarchitecture.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class FotomApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>
    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    val component: AppComponent by lazy {
        DaggerAppComponent.builder()
                .application(this)
                .build()

    }

    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        component.inject(this)
    }
}