package com.fotommvvmcleanarchitecture.ui.base

import android.app.Activity
import android.content.Context
import com.fotommvvmcleanarchitecture.di.qualifiers.ForActivity
import com.fotommvvmcleanarchitecture.di.scopes.PerActivity
import dagger.Binds
import dagger.Module

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Module
abstract class BaseActivityModule {

    @PerActivity
    @ForActivity
    @Binds
    abstract fun bindsContext(activity: Activity): Context
}