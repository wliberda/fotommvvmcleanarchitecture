package com.fotommvvmcleanarchitecture.ui.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.fotommvvmcleanarchitecture.utils.*
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by wojciechliberda on 21/01/2018.
 */
abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector, LifecycleOwner {
    var toastMessage : Toast? = null

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingFragmentInjector
    val registry = LifecycleRegistry(this)

    override fun getLifecycle(): Lifecycle = registry

    abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }


    internal fun showToastMessage(message: Int) {
        toastMessage?.cancel()
        toastMessage = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        toastMessage?.show()
    }


}
