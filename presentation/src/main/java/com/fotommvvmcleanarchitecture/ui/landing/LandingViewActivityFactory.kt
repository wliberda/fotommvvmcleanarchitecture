package com.fotommvvmcleanarchitecture.ui.landing

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
/**
 * Created by wojciechliberda on 21/01/2018.
 */
class LandingViewActivityFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LandingViewModel::class.java)) {
            return LandingViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}