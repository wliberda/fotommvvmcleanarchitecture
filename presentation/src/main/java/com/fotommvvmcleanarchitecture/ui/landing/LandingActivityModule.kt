package com.fotommvvmcleanarchitecture.ui.landing

import com.fotommvvmcleanarchitecture.ui.base.BaseActivityModule
import com.fotommvvmcleanarchitecture.di.scopes.PerActivity
import dagger.Module
import dagger.Provides

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Module(includes = [(BaseActivityModule::class)])
class LandingActivityModule {


    @Provides
    @PerActivity
    fun provideLandingViewModel() =
            LandingViewActivityFactory()



}