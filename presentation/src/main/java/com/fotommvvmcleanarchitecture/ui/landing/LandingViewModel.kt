package com.fotommvvmcleanarchitecture.ui.landing

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.fotommvvmcleanarchitecture.mapper.UserModelDataMapper
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class LandingViewModel : ViewModel() {


    val isSignInButtonEnable = MutableLiveData<Boolean>()

    fun btnLogInClicked() {
        isSignInButtonEnable.postValue(true)

    }


}