package com.fotommvvmcleanarchitecture.ui.landing

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.fotommvvmcleanarchitecture.R
import com.fotommvvmcleanarchitecture.ui.base.BaseActivity
import com.fotommvvmcleanarchitecture.ui.login.LoginActivity
import com.network.token.TokenAuthenticator
import com.sharedprefrences.Session
import domain.com.user.User
import kotlinx.android.synthetic.main.landing_activity.*
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * Created by wojciechliberda on 21/01/2018.
 */
class LandingActivity : BaseActivity() {
    @Inject
    lateinit var landingViewModelFactory: LandingViewActivityFactory

    private lateinit var viewModel: LandingViewModel

    override fun getLayoutId(): Int = R.layout.landing_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, landingViewModelFactory).get(LandingViewModel::class.java)
        setListeners()
        setObservers()
    }

    private fun setListeners() {
        btnLogIn.setOnClickListener { viewModel.btnLogInClicked() }
    }

    private fun setObservers() {
        viewModel.isSignInButtonEnable.observe(this, android.arch.lifecycle.Observer {
            startLoginActivity()
        })
    }

    private fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }


}