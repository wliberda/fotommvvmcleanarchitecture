package com.fotommvvmcleanarchitecture.ui.chooseteam

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.fotommvvmcleanarchitecture.vo.Response
import domain.com.team.Team
import domain.com.team.usecase.GetTeamsForUserUseCase
import io.reactivex.SingleObserver
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import timber.log.Timber
import java.util.*

/**
 * Created by wojciechliberda on 23/02/2018.
 */
class ChooseTeamViewModel(private val getTeamsForUserUseCase: GetTeamsForUserUseCase) : ViewModel() {
    var teams = MutableLiveData<Response<List<Team>>>()

    fun getTeams() {
        teams.value = Response.loading()
        getTeamsForUserUseCase.execute(SingleObserver(), UUID.randomUUID())
    }


    inner class SingleObserver : DisposableSingleObserver<List<Team>>() {

        override fun onSuccess(t: List<Team>) {
            teams.value = Response.success(t)
            Timber.d("Success ${t.size}")
        }

        override fun onError(e: Throwable) {
            teams.value = Response.error(e)
            Timber.e(e)
        }
    }
}