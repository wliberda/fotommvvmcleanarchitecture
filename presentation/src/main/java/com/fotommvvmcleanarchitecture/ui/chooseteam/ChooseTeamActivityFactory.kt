package com.fotommvvmcleanarchitecture.ui.chooseteam

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import domain.com.team.usecase.GetTeamsForUserUseCase

/**
 * Created by wojciechliberda on 23/02/2018.
 */
class ChooseTeamActivityFactory(private val getTeamSForUserUseCase: GetTeamsForUserUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChooseTeamViewModel::class.java))
            return ChooseTeamViewModel(getTeamSForUserUseCase) as T
        throw IllegalArgumentException("Unknow ViewModel class")
    }
}