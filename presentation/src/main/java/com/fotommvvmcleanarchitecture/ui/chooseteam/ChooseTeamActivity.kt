package com.fotommvvmcleanarchitecture.ui.chooseteam

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.fotommvvmcleanarchitecture.R
import com.fotommvvmcleanarchitecture.ui.base.BaseActivity
import javax.inject.Inject

/**
 * Created by wojciechliberda on 23/02/2018.
 */
class ChooseTeamActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ChooseTeamActivityFactory
    lateinit var viewModel: ChooseTeamViewModel

    override fun getLayoutId(): Int = R.layout.activity_choose_team

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ChooseTeamViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.getTeams()
    }
}