package com.fotommvvmcleanarchitecture.ui.chooseteam

import com.fotommvvmcleanarchitecture.di.scopes.PerActivity
import com.fotommvvmcleanarchitecture.ui.base.BaseActivityModule
import dagger.Module
import dagger.Provides
import domain.com.team.repository.TeamRepository
import domain.com.team.usecase.GetTeamsForUserUseCase

/**
 * Created by wojciechliberda on 23/02/2018.
 */
@Module(includes = [BaseActivityModule::class])
class ChooseTeamActivityModule {


    @Provides
    @PerActivity
    fun provideGetTeamsForUseCase(teamRepository: TeamRepository) = GetTeamsForUserUseCase(teamRepository)
    @Provides
    @PerActivity
    fun provideChooseTeamViewModel(getTeamsForUserUseCase: GetTeamsForUserUseCase) = ChooseTeamActivityFactory(getTeamsForUserUseCase)




}