package com.fotommvvmcleanarchitecture.ui.chooseteam

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fotommvvmcleanarchitecture.R
import domain.com.team.Team
import android.support.v4.widget.*
import android.widget.TextView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import android.content.Context
/**
 * Created by wojciechliberda on 19/03/2018.
 */
class ChooseTeamAdapter(val data: List<Team>, val context: Context) : RecyclerView.Adapter<ChooseTeamAdapter.TeamViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder =
        LayoutInflater.from(parent.context).inflate(R.layout.adapter_team, parent, false).run {
            TeamViewHolder(this)
        }


    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.apply {
            data[position].let { team ->
//                Glide.with(context)
//                        .load(team.avatarUrl)
//                        .into(civAvatar)
                teamName.text = team.teamName
            }
        }
    }

    override fun getItemCount(): Int = data.size


    inner class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val civAvatar = view.findViewById<CircleImageView>(R.id.civAvatar)!!
        val teamName = view.findViewById<TextView>(R.id.teamName)!!
    }
}