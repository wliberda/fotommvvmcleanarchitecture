package com.fotommvvmcleanarchitecture.ui.login

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.fotommvvmcleanarchitecture.mapper.UserModelDataMapper
import domain.com.login.LoginUseCase
import domain.com.user.usecase.GetUserUseCase

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class LoginViewActivityFactory(private val loginUseCase: LoginUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java))
            return LoginViewModel(loginUseCase) as T
        throw IllegalArgumentException("Unknow ViewModel class")
    }

}