package com.fotommvvmcleanarchitecture.ui.login

import com.fotommvvmcleanarchitecture.ui.base.BaseActivityModule
import com.fotommvvmcleanarchitecture.di.scopes.PerActivity
import dagger.Module
import dagger.Provides
import domain.com.base.EmailValidator
import domain.com.login.LoginUseCase
import domain.com.login.repository.LoginRepository
import domain.com.user.repository.UserRepository
import domain.com.user.usecase.GetUserUseCase

/**
 * Created by wojciechliberda on 06/02/2018.
 */
@Module(includes = [BaseActivityModule::class])
class LoginActivityModule {


    @Provides
    @PerActivity
    fun provideLoginUseCase(loginRepository: LoginRepository,
                            userRepository: UserRepository,
                            emailValidator: EmailValidator) = LoginUseCase(loginRepository, userRepository, emailValidator)


    @Provides
    @PerActivity
    fun provideViewModel(loginUseCase: LoginUseCase) =
            LoginViewActivityFactory(loginUseCase)
}