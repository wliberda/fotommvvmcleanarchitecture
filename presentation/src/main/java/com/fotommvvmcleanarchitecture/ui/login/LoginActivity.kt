package com.fotommvvmcleanarchitecture.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.fotommvvmcleanarchitecture.R
import com.fotommvvmcleanarchitecture.ui.base.BaseActivity
import com.fotommvvmcleanarchitecture.ui.chooseteam.ChooseTeamActivity
import com.fotommvvmcleanarchitecture.utils.ShortSnack
import com.fotommvvmcleanarchitecture.vo.Resource
import com.fotommvvmcleanarchitecture.vo.Status
import com.network.NetworkException
import domain.com.base.exceptions.EmailValidationException
import domain.com.login.exception.PasswordEmptyOrShortException
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class LoginActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: LoginViewActivityFactory
    lateinit var viewModel: LoginViewModel


    override fun getLayoutId(): Int = R.layout.activity_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        setActionListener()
        setObservers()
    }

    private fun setActionListener() {
        btnLogIn.setOnClickListener {
            viewModel.btnLogInClicked(email = etEmailAddress.text.toString(),
                    password = etPassword.text.toString())
        }
    }

    private fun setObservers() {
        viewModel.isLoginSuccess.observe(this, Observer {
            when (it?.status) {
                Status.ERROR -> {
                    progressBar.visibility = GONE
                    val resourceString = when (it.error) {
                        is PasswordEmptyOrShortException ->  R.string.error_empty_or_short_password
                        is EmailValidationException -> R.string.error_email_not_valid
                        is NetworkException -> R.string.error_network_conntection
                        else -> R.string.error_occurs
                    }
                    showToastMessage(resourceString)
                }
                Status.LOADING -> {
                    progressBar.visibility = VISIBLE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = GONE
                    startChooseTeamActivity()
                }
            }
        })
    }

    private fun startChooseTeamActivity() {
        val intent = Intent(this, ChooseTeamActivity::class.java)
        startActivity(intent)
    }
}