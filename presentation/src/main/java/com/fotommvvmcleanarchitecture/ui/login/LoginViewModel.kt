package com.fotommvvmcleanarchitecture.ui.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.fotommvvmcleanarchitecture.vo.Response
import domain.com.login.Login
import domain.com.login.LoginUseCase
import io.reactivex.observers.DisposableCompletableObserver
import timber.log.Timber

/**
 * Created by wojciechliberda on 06/02/2018.
 */
class LoginViewModel(private val loginUseCase: LoginUseCase) : ViewModel() {

    var isLoginSuccess = MutableLiveData<Response<Boolean>>()

    fun btnLogInClicked(email: String, password: String) {
        isLoginSuccess.value = Response.loading()
        loginUseCase.execute(CompletableObserver(), Login("password", email, password))
    }

    inner class CompletableObserver  : DisposableCompletableObserver() {
        override fun onComplete() {
            isLoginSuccess.value = Response.success(true)
            Timber.d("Success")
        }

        override fun onError(e: Throwable) {
            isLoginSuccess.value = Response.error(e)
            Timber.d(e)
        }

    }
    override fun onCleared() {
        super.onCleared()
        loginUseCase.dispose()
    }
}