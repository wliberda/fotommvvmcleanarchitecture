package com.fotommvvmcleanarchitecture.di

import android.app.Application
import com.fotommvvmcleanarchitecture.FotomApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Singleton
@Component(modules = [(AppModule::class),(AndroidInjectionModule::class), (AndroidSupportInjectionModule::class), (ActivityBuilder::class)])
        interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(fotomApplication: FotomApplication)
}