package com.fotommvvmcleanarchitecture.di

import android.util.Base64
import com.base.Constants
import com.fotommvvmcleanarchitecture.BuildConfig
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import com.network.UserAPI
import com.network.token.TokenAuthenticator
import com.network.token.TokenInterceptor
import com.sharedprefrences.Session
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 06/02/2018.
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().addSerializationExclusionStrategy(object : ExclusionStrategy {
        override fun shouldSkipField(fieldAttributes: FieldAttributes): Boolean {
            val expose = fieldAttributes.getAnnotation(Expose::class.java)
            return expose != null && !expose.serialize
        }

        override fun shouldSkipClass(aClass: Class<*>): Boolean = false
    })
            .addDeserializationExclusionStrategy(object : ExclusionStrategy {
                override fun shouldSkipField(fieldAttributes: FieldAttributes): Boolean {
                    val expose = fieldAttributes.getAnnotation(Expose::class.java)
                    return expose != null && !expose.deserialize
                }

                override fun shouldSkipClass(aClass: Class<*>): Boolean = false
            })
            .create()

    @Singleton
    @Named(Constants.HTTP_CLIENT_LOGIN_BUILDER)
    @Provides
    fun provideOkHttpClientBuilderLogin() = OkHttpClient.Builder()

    @Singleton
    @Named(Constants.HTTP_CLIENT_TOKENS_BUILDER)
    @Provides
    fun provideOkHttpClientBuilder(tokenInterceptor: TokenInterceptor,
                               tokenAuthenticator: TokenAuthenticator,
                               @Named(Constants.HTTP_CLIENT_LOGIN_BUILDER) okHttpClientBuilder: OkHttpClient.Builder): OkHttpClient.Builder =
            Dispatcher().run {
                maxRequests = 1
                okHttpClientBuilder
                        .authenticator(tokenAuthenticator)
                        .addInterceptor(tokenInterceptor)
                        .dispatcher(this)
            }


    @Singleton
    @Named(Constants.HTTP_CLIENT_TOKENS)
    @Provides
    fun provideOkHttpClient(@Named(Constants.HTTP_CLIENT_TOKENS_BUILDER) okHttpClient: OkHttpClient.Builder): OkHttpClient {
        okHttpClient
                .interceptors().add(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.HEADERS
                })
        return okHttpClient
                .build()!!
    }
    @Singleton
    @Named(Constants.HTTP_CLIENT_LOGIN)
    @Provides
    fun provideOkHttpClientLogin(@Named(Constants.HTTP_CLIENT_LOGIN_BUILDER) okHttpClient: OkHttpClient.Builder): OkHttpClient {
        okHttpClient
                .interceptors().add(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.HEADERS
                })
        return okHttpClient.build()!!
    }


    @Singleton
    @Named(Constants.RETROFIT_BUILDER_TOKEN)
    @Provides
    fun provideRetrofitBuilder(gson: Gson, @Named(Constants.HTTP_CLIENT_TOKENS) okHttpClient: OkHttpClient) = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)!!

    @Singleton
    @Named(Constants.RETROFIT_BUILDER_LOGIN)
    @Provides
    fun provideRetrofitBuilderLogin(gson: Gson, @Named(Constants.HTTP_CLIENT_LOGIN) okHttpClient: OkHttpClient) = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)!!


    @Singleton
    @Provides
    fun provideTokenAuthenticator(session: Session, @Named(Constants.USER_API) userAPI: UserAPI) = TokenAuthenticator(session, userAPI)

    @Singleton
    @Provides
    fun provideTokenInterceptor(session: Session) = TokenInterceptor(session)


    @Singleton
    @Provides
    fun provideRetrofitLogin(@Named(Constants.RETROFIT_BUILDER_LOGIN) retrofitBuilder: Retrofit.Builder) = retrofitBuilder.build()!!


    @Provides
    @Named(Constants.LOGIN_USER_API)
    @Singleton
    fun provideUserApiForLogin(@Named(Constants.HTTP_CLIENT_LOGIN_BUILDER) httpClient: OkHttpClient.Builder,
                               @Named(Constants.RETROFIT_BUILDER_LOGIN) builder: Retrofit.Builder): UserAPI {
        val credentials = "iosannoapp:iosannoapppass"
        val basic = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                    .header("Content-Type", "charset=utf-8; application/x-www-form-urlencoded")
                    .header("Authorization", basic)
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        val client = httpClient.build()
        val retrofit = builder.client(client).build()
        return retrofit.create(UserAPI::class.java)
    }

    @Singleton
    @Named(Constants.USER_API)
    @Provides
    fun provideUserApi(retrofit: Retrofit): UserAPI =
            retrofit.create(UserAPI::class.java)

    @Singleton
    @Named(Constants.USER_API_TOKENS)
    @Provides
    fun provideUserApiWithTokens(@Named(Constants.RETROFIT_BUILDER_TOKEN) retrofitBuilder: Retrofit.Builder) : UserAPI =
            retrofitBuilder.build()
                    .create(UserAPI::class.java)

}