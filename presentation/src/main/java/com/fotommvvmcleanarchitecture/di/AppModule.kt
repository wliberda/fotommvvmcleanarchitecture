package com.fotommvvmcleanarchitecture.di

import android.app.Application
import android.content.Context
import com.fotommvvmcleanarchitecture.di.qualifiers.ForApplication
import com.sharedprefrences.Session
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Module(includes = [(AppDBModule::class), (NetworkModule::class), (UtilsModule::class), (MapperModule::class)])
class AppModule {

    @Singleton
    @ForApplication
    @Provides
    fun provideContext(app: Application): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideSession(@ForApplication context: Context) = Session(context)

}