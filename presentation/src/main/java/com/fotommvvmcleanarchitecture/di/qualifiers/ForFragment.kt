package com.fotommvvmcleanarchitecture.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by wojciechliberda on 03/02/2018.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ForFragment