package com.fotommvvmcleanarchitecture.di

import android.content.Context
import com.base.Constants
import com.db.FotomDB
import com.fotommvvmcleanarchitecture.di.qualifiers.ForApplication
import com.login.LoginRepositoryImpl
import com.network.UserAPI
import com.sharedprefrences.Session
import com.teamdata.TeamDAO
import com.teamdata.TeamModelMapperImpl
import com.teamdata.TeamRepositoryImpl
import com.teamdata.UserTeamJoinDao
import com.userdata.UserDAO
import com.userdata.UserModelMapperImpl
import com.userdata.UserRepositoryImpl
import dagger.Module
import dagger.Provides
import domain.com.login.repository.LoginRepository
import domain.com.team.repository.TeamRepository
import domain.com.user.repository.UserRepository
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Module
class AppDBModule {

    @Singleton
    @Provides
    fun provideDatabase(@ForApplication context: Context): FotomDB = FotomDB.getInstance(context)

    @Singleton
    @Provides
    fun provideUserMapper() = UserModelMapperImpl()

    @Singleton
    @Provides
    fun provideUserDAO(fotomDB: FotomDB) = fotomDB.userDao()


    @Singleton
    @Provides
    fun provideUserRepository(userDAO: UserDAO,
                              mapper: UserModelMapperImpl,
                              @Named(Constants.USER_API_TOKENS) userAPI: UserAPI): UserRepository =
            UserRepositoryImpl(userDAO, userAPI, mapper)


    @Singleton
    @Provides
    fun provideUserTeamJoint(fotomDB: FotomDB) = fotomDB.userTeamJoinDao()

    @Singleton
    @Provides
    fun provideTeamDAO(fotomDB: FotomDB) = fotomDB.teamDao()


    @Singleton
    @Provides
    fun provideTeamMapper() = TeamModelMapperImpl()

    @Singleton
    @Provides
    fun provideTeamRepository(teamDAO: TeamDAO,
                              mapper: TeamModelMapperImpl,
                              userTeamJoinDao: UserTeamJoinDao,
                              @Named(Constants.USER_API_TOKENS) userAPI: UserAPI,
                              session: Session): TeamRepository =
            TeamRepositoryImpl(teamDAO, userTeamJoinDao, userAPI, mapper, session)

    @Singleton
    @Provides
    fun provideLoginRepository(@Named(Constants.LOGIN_USER_API) userAPI: UserAPI, session: Session): LoginRepository = LoginRepositoryImpl(userAPI, session)


}