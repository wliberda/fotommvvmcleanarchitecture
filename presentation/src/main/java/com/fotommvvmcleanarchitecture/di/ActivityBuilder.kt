package com.fotommvvmcleanarchitecture.di

import com.fotommvvmcleanarchitecture.ui.landing.LandingActivity
import com.fotommvvmcleanarchitecture.ui.landing.LandingActivityModule
import com.fotommvvmcleanarchitecture.ui.login.LoginActivity
import com.fotommvvmcleanarchitecture.ui.login.LoginActivityModule
import com.fotommvvmcleanarchitecture.di.scopes.PerActivity
import com.fotommvvmcleanarchitecture.ui.chooseteam.ChooseTeamActivity
import com.fotommvvmcleanarchitecture.ui.chooseteam.ChooseTeamActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by wojciechliberda on 21/01/2018.
 */
@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [LandingActivityModule::class])
    abstract fun contributeLandingActivity(): LandingActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ChooseTeamActivityModule::class])
    abstract fun contributeChooseTeamActivity(): ChooseTeamActivity

}