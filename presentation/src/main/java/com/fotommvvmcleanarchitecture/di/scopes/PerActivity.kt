package com.fotommvvmcleanarchitecture.di.scopes

import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Created by wojciechliberda on 25/11/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity