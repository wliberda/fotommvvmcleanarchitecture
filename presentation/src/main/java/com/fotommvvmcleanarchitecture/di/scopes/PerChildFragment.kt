package com.fotommvvmcleanarchitecture.di.scopes

import javax.inject.Scope

/**
 * Created by wojciechliberda on 26/11/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerChildFragment