package com.fotommvvmcleanarchitecture.di

import com.fotommvvmcleanarchitecture.utils.DateUtils
import com.fotommvvmcleanarchitecture.utils.DateUtilsImpl
import dagger.Module
import dagger.Provides
import domain.com.base.EmailValidator
import domain.com.base.EmailValidatorImpl
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 18/02/2018.
 */
@Module
class UtilsModule {


    @Provides
    @Singleton
    fun provideDateUtils(): DateUtils = DateUtilsImpl()

    @Provides
    @Singleton
    fun provideEmailValidator(): EmailValidator = EmailValidatorImpl()
}