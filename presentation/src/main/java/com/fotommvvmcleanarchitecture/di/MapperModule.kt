package com.fotommvvmcleanarchitecture.di

import com.fotommvvmcleanarchitecture.mapper.UserModelDataMapper
import com.fotommvvmcleanarchitecture.utils.DateUtils
import dagger.Module

/**
 * Created by wojciechliberda on 18/02/2018.
 */
@Module
class MapperModule {
    fun provideUserModelMapper(dateUtils: DateUtils) = UserModelDataMapper(dateUtils)
}