package com.fotommvvmcleanarchitecture.model

import org.threeten.bp.LocalDateTime
import java.util.*

/**
 * Created by wojciechliberda on 17/02/2018.
 */
data class UserModel(val uuid: UUID? = null,
                     val changed: LocalDateTime? = null,
                     val created: LocalDateTime? = null,
                     var firstName: String? = null,
                     var lastName: String? = null,
                     var email: String? = null,
                     val text: String? = null,
                     var userName: String? = null,
                     var occupation: String? = null,
                     val attachment: String? = null) {
}