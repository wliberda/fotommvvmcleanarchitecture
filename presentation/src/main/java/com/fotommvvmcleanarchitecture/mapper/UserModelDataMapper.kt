package com.fotommvvmcleanarchitecture.mapper

import com.fotommvvmcleanarchitecture.model.UserModel
import com.fotommvvmcleanarchitecture.utils.DateUtils
import domain.com.user.User
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 17/02/2018.
 */
@Singleton
class UserModelDataMapper @Inject constructor(val dateUtils: DateUtils) {

    fun transform(user: User): UserModel = user.run {
        val createdLocalDate = dateUtils.parseToLocalDateTimeWithDateFormat(created!!)
        val changedLocalDate = dateUtils.parseToLocalDateTimeWithDateFormat(changed!!)
        UserModel(uuid, changedLocalDate, createdLocalDate,  firstName, lastName, email, text, userName, occupation, attachment)
    }


    fun transformCollection(userCollection: Collection<User>): Collection<UserModel> {
        return if(userCollection.isEmpty()) emptyList()
        else {
            arrayListOf<UserModel>().apply {
                userCollection.forEach {
                    this.add(transform(it))
                }
            }
        }
    }


}