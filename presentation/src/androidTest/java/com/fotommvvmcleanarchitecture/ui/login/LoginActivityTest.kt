package com.fotommvvmcleanarchitecture.ui.login

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.fotommvvmcleanarchitecture.R
import kotlinx.android.synthetic.main.activity_login.view.*
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.*
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import org.junit.Before
import com.nhaarman.mockito_kotlin.*

/**
 * Created by wojciechliberda on 23/02/2018.
 */
@RunWith(AndroidJUnit4::class)
class LoginActivityTest {
    @get:Rule
    val activity = ActivityTestRule(LoginActivity::class.java)
    lateinit var viewModel: LoginViewModel

    companion object {
        val email = "wliberda@omnis.pl"
        val password = "password"
    }

    @Before
    fun setUp() {
        viewModel = mock()
    }

    @Test
    fun `when loginClicked then call methods with email and password`() {
        onView(withId(R.id.etEmailAddress)).perform(ViewActions.typeText(email))
        onView(withId(R.id.etPassword)).perform(ViewActions.typeText(password))
        assertTrue(true)
//        onView(withId(R.id.btnLogIn)).perform(click()).check(viewModel.btnLogInClicked(email, password))
    }

}